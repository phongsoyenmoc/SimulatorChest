﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class GameManager : Singleton<GameManager>
{
	public enum KindCard { Common = 0, Rare = 1, Epic = 2, Legendary = 3 }
	public enum KindOfChest
	{
		Wood,
		Silver,
		Gold,
		Magic,
		SuperMagic
	}
	public Dictionary<int, ChestData> ChestData = new Dictionary<int, global::ChestData> ();
	public Dictionary<int, RewardChestData> DataCoinRewards = new Dictionary<int, RewardChestData> ();
	public Dictionary<int, RewardChestData> DataExpRewards = new Dictionary<int, RewardChestData> ();
	public Dictionary<int, CoinGemInChestShopData> DataCoinGemInChestShop = new Dictionary<int, CoinGemInChestShopData> ();
	public Dictionary<int, ChestInShopData> DataChestInShop = new Dictionary<int, ChestInShopData> ();
	public Dictionary<int, CardInShopData> DataCardInShop = new Dictionary<int, CardInShopData> ();

	[Header ("Setting")]
	public int NumberOpenChestCamplan;
	public KindOfChest kindChestInShop;
	public int NumberOpenChestInShop;
	public int MapLevelPassForOpenChestInShop;

	public ChestRewardController ChestRewardData;
	public ChestShopController ChestShopData;
	public CardInShopController CardShopData;
	public string UserNamePC;
	public int maxCollum = 20;

	public List<int> CurrentListIdCard;
	public List<int> CurrentListNumberCard;

	public Dictionary<int, int> CurrentIdNummberCard = new Dictionary<int, int> ();
	public int MinValueRandomForAlgorithmChest = 10;


	[HideInInspector]
	public Dictionary<int, int> IdNumberCardEnemyChestInShop;
	[HideInInspector]
	public List<int> CoinForChestShop;
	[HideInInspector]
	public List<int> GemsForChestShop;



	[EasyButtons.Button]
	public void InitialData ()
	{

		CurrentIdNummberCard = UltiGenerics.MakeDictionary (CurrentListIdCard, CurrentListNumberCard);

		ChestData = CSVReaderChestInfomation.Read ("ChestData/ChestDropRate");
		ChestRewardData.ChestData = ChestData;
		DataCoinRewards = ReadCSVCoinExpRewardChest.Read ("ChestData/CoinChest");
		DataExpRewards = ReadCSVCoinExpRewardChest.Read ("ChestData/ExpChest");
		DataChestInShop = CSVReaderChestInShopData.Read ("ChestData/ChestShop/ChestShop");
		ChestShopData.ChestData = DataChestInShop;
		DataCardInShop = CSVReaderCardInShopData.Read ("CardData/CardInShop");
		DataCoinGemInChestShop = CSVReaderCoinGemInChestShopData.Read ("ChestData/ChestShop/CoinAndGemChest/CoinAndGemShop");
	}


	//[EasyButtons.Button]
	//public void DebugLoadData ()
	//{
	//	Debug.Log ("-------------------------------------");
	//	foreach (var item in ChestData.Keys)
	//	{
	//		Debug.Log ("Keys : " + item + "---RareCards[0]" + ChestData[item].RareCards[0]);
	//	}


	//	Debug.Log ("-------------------------------------");
	//	foreach (var item in DataCoinRewards.Keys)
	//	{
	//		Debug.Log ("keys : " + item + "--- level : " + DataCoinRewards[item].Level + "--- coin : " + DataCoinRewards[item].Value);
	//	}


	//	Debug.Log ("-------------------------------------");
	//	foreach (var item in DataExpRewards.Keys)
	//	{
	//		Debug.Log ("keys : " + item + "--- level : " + DataExpRewards[item].Level + "--- exp : " + DataExpRewards[item].Value);
	//	}

	//	Debug.Log ("-------------------------------------");
	//	foreach (var item in DataChestInShop.Keys)
	//	{
	//		Debug.Log ("keys : " + item + "--- common rate" + DataChestInShop[item].FreeCommonRate + "--- rare rate : " + DataChestInShop[item].FreeRareRate + "---epic rate : " + DataChestInShop[item].FreeEpicRate + "---legend rate : " + DataChestInShop[item].FreeLegendRate + "\n");
	//	}
	//	Debug.Log ("-------------------------------------");
	//	foreach (var item in DataCardInShop.Keys)
	//	{
	//		Debug.Log ("Commonn card in shop[0] = " + DataCardInShop[item].CommonCards[0] + "---Commonn card in shop[1] = " + DataCardInShop[item].CommonCards[0]);
	//	}
	//}



	private void MakeCSVData (string nameFile, Dictionary<int, DataContainer> dataChest)
	{
		string path = System.Environment.GetFolderPath (System.Environment.SpecialFolder.Desktop) + @"\" + nameFile + @".csv";
		string delimiter = ", ";
		if (File.Exists (path))
		{
			File.Delete (path);
		}
		if (!File.Exists (path))
		{
			string createText = "Stt" + delimiter;
			// Create a file to write to.
			for (int i = 1; i <= maxCollum; i++)
			{
				createText += "column " + i + delimiter;
			}
			createText += Environment.NewLine;
			File.WriteAllText (path, createText);

			string appendText = "";

			int j = 0;
			foreach (var item in dataChest.Keys)
			{
				j++;
				appendText = j + delimiter;
				foreach (var item2 in dataChest[item].IdNumberCard.Keys)
				{
					appendText += item2 + "&" + dataChest[item].IdNumberCard[item2] + delimiter;
				}
				appendText += dataChest[item].coin + delimiter;
				appendText += Environment.NewLine;
				File.AppendAllText (path, appendText);
			}
		}
	}


	private void MakeCSVAverage (string nameFile, Dictionary<int, DataContainer> dataChest)
	{
		string path = System.Environment.GetFolderPath (System.Environment.SpecialFolder.Desktop) + @"\" + nameFile + @".csv";
		string delimiter = ", ";
		if (File.Exists (path))
		{
			File.Delete (path);
		}
		if (!File.Exists (path))
		{
			List<int> tempIds = new List<int> ();
			List<int> numberOfIds = new List<int> ();
			int sumCoin = 0;
			foreach (var item in dataChest.Keys)
			{
				foreach (var item2 in dataChest[item].IdNumberCard.Keys)
				{
					if (!UltiGenerics.ExistIdInList (item2, tempIds))
					{
						tempIds.Add (item2);
					}
				}
				sumCoin += dataChest[item].coin;
			}

			foreach (var item0 in tempIds)
			{
				int sum = 0;
				foreach (var item in dataChest.Keys)
				{
					foreach (var item2 in dataChest[item].IdNumberCard.Keys)
					{
						if (item0 == item2)
						{
							sum += dataChest[item].IdNumberCard[item2];
						}
					}
				}
				numberOfIds.Add (sum);
			}


			string createText = "Stt" + delimiter;
			// Create a file to write to.
			foreach (var item in tempIds)
			{
				createText += item + delimiter;
			}
			createText += "total coin " + delimiter;
			createText += Environment.NewLine;
			File.WriteAllText (path, createText);

			string appendText = "";
			appendText = 1 + delimiter;
			foreach (var item in numberOfIds)
			{
				appendText += item + delimiter;
			}
			appendText += sumCoin + delimiter;
			appendText += Environment.NewLine;
			File.AppendAllText (path, appendText);
		}



	}



	[EasyButtons.Button]
	public void CaculateChestCamplan ()
	{
		Dictionary<int, DataContainer> dataChest = new Dictionary<int, DataContainer> ();

		for (int i = 1; i <= NumberOpenChestCamplan; i++)
		{
			Dictionary<int, int> temp = LoadCardRewardData (i, KindOfChest.Wood, KindOfChest.Silver, KindOfChest.Gold);
			UltiGenerics.MergingDictionary (ref CurrentIdNummberCard, temp);
			DataContainer data = new DataContainer (temp, DataCoinRewards[i].Value);
			dataChest.Add (i, data);
		}

		MakeCSVData ("ChestCamplan", dataChest);
		MakeCSVAverage ("AverageChestCamplan", dataChest);
	}


	[EasyButtons.Button]
	public void CaculateChestInShop ()
	{
		Dictionary<int, DataContainer> dataChest = new Dictionary<int, DataContainer> ();
		for (int i = 1; i <= NumberOpenChestInShop; i++)
		{
			LoadCardRewardDataShop ((MapLevelPassForOpenChestInShop - 1) / 10 + 1, kindChestInShop);
			LoadCoinWithChestInShop ((MapLevelPassForOpenChestInShop - 1) / 10 + 1, kindChestInShop);
			DataContainer data = new DataContainer (IdNumberCardEnemyChestInShop, CoinForChestShop[0]);
			dataChest.Add (i, data);
		}

		MakeCSVData ("ChestShop_" + ConvertKindChestShop (kindChestInShop), dataChest);
		MakeCSVAverage ("AverageChestShop_" + ConvertKindChestShop (kindChestInShop), dataChest);
	}



	[System.Serializable]
	public class DataContainer
	{
		public Dictionary<int, int> IdNumberCard;
		public int coin;
		public DataContainer (Dictionary<int, int> IdNumberCard, int coin)
		{
			this.IdNumberCard = IdNumberCard;
			this.coin = coin;
		}
	}



	#region NumberCard
	public List<int> GetNumberCardInCardCollection (List<int> containerId)
	{
		List<int> tempoContainer = new List<int> ();
		List<int> tempoListId = UltiGenerics.GetKeysDictionary (CurrentIdNummberCard);
		foreach (var item in containerId)
		{
			if (UltiGenerics.ExistIdInList (item, tempoListId))
			{
				tempoContainer.Add (CurrentIdNummberCard[item]);
			}
			else
			{
				tempoContainer.Add (0);
			}
		}
		return tempoContainer;
	}

	#endregion



	public Dictionary<int, int> LoadCardRewardData (int _level,
		GameManager.KindOfChest kindOfChestNormal,
		GameManager.KindOfChest kindOfChest5Level,
		GameManager.KindOfChest kindOfChest10Level)
	{
		Dictionary<int, int> listEnemyIdAndNum = new Dictionary<int, int> ();
		if (_level % 5 != 0)
		{
			listEnemyIdAndNum = LoadCardRewardCamplain ((_level - 1) / 10 + 1, kindOfChestNormal);
		}
		else if (_level % 5 == 0 && _level % 10 != 0)
		{
			listEnemyIdAndNum = LoadCardRewardCamplain ((_level - 1) / 10 + 1, kindOfChest5Level, false);
		}
		else
		{
			listEnemyIdAndNum = LoadCardRewardCamplain ((_level - 1) / 10 + 1, kindOfChest10Level, false);
		}

		return listEnemyIdAndNum;
	}



	private Dictionary<global::GameManager.KindCard, List<int>> GetRangeCountCardForChestReward (int index, GameManager.KindOfChest _kindChest)
	{
		ChestData _data0 = ChestData[index];
		List<int> _listNum_Range_DatasCommon = new List<int> ();
		List<int> _listNum_Range_DatasRare = new List<int> ();
		List<int> _listNum_Range_DatasEpic = new List<int> ();
		List<int> _listNum_Range_DatasLegend = new List<int> ();

		switch (_kindChest)
		{
			case GameManager.KindOfChest.Wood:
				_listNum_Range_DatasCommon = _data0.NumberCardCommonChestWood;
				_listNum_Range_DatasRare = _data0.NumberCardRareChestWood;
				_listNum_Range_DatasEpic = _data0.NumberCardEpicChestWood;
				_listNum_Range_DatasLegend = _data0.NumberCardLegendChestWood;

				break;
			case GameManager.KindOfChest.Silver:
				_listNum_Range_DatasCommon = _data0.NumberCardCommonChestSilver;
				_listNum_Range_DatasRare = _data0.NumberCardRareChestSilver;
				_listNum_Range_DatasEpic = _data0.NumberCardEpicChestSilver;
				_listNum_Range_DatasLegend = _data0.NumberCardLegendChestSilver;
				break;
			case GameManager.KindOfChest.Gold:
				_listNum_Range_DatasCommon = _data0.NumberCardCommonChestGold;
				_listNum_Range_DatasRare = _data0.NumberCardRareChestGold;
				_listNum_Range_DatasEpic = _data0.NumberCardEpicChestGold;
				_listNum_Range_DatasLegend = _data0.NumberCardLegendChestGold;
				break;
		}
		Dictionary<global::GameManager.KindCard, List<int>> _tempDictionary = new Dictionary<global::GameManager.KindCard, List<int>> ();
		_tempDictionary.Add (KindCard.Common, _listNum_Range_DatasCommon);
		_tempDictionary.Add (KindCard.Rare, _listNum_Range_DatasRare);
		_tempDictionary.Add (KindCard.Epic, _listNum_Range_DatasEpic);
		_tempDictionary.Add (KindCard.Legendary, _listNum_Range_DatasLegend);
		return _tempDictionary;
	}

	public Dictionary<int, int> LoadCardRewardCamplain (int index, GameManager.KindOfChest kindOfChestReward, bool IsAttachRandomlizeMinWood = true)
	{
		Dictionary<KindCard, List<int>> RangeCountCardOpenForChestReward = GetRangeCountCardForChestReward (index, kindOfChestReward);

		int _countCardCommon = 0;
		int _countCardRare = 0;
		int _countCardEpic = 0;
		int _countCardLegendary = 0;

		if (IsAttachRandomlizeMinWood)
		{
			_countCardCommon = UltiGenerics.RandomlizeMin (RangeCountCardOpenForChestReward[KindCard.Common][1], RangeCountCardOpenForChestReward[KindCard.Common][2] + 1);
			_countCardRare = UltiGenerics.RandomlizeMin (RangeCountCardOpenForChestReward[KindCard.Rare][1], RangeCountCardOpenForChestReward[KindCard.Rare][2] + 1);
			_countCardEpic = UltiGenerics.RandomlizeMin (RangeCountCardOpenForChestReward[KindCard.Epic][1], RangeCountCardOpenForChestReward[KindCard.Epic][2] + 1);
			_countCardLegendary = UltiGenerics.RandomlizeMin (RangeCountCardOpenForChestReward[KindCard.Legendary][1], RangeCountCardOpenForChestReward[KindCard.Legendary][2] + 1);
		}
		else
		{
			_countCardCommon = UnityEngine.Random.Range (RangeCountCardOpenForChestReward[KindCard.Common][1], RangeCountCardOpenForChestReward[KindCard.Common][2] + 1);
			_countCardRare = UnityEngine.Random.Range (RangeCountCardOpenForChestReward[KindCard.Rare][1], RangeCountCardOpenForChestReward[KindCard.Rare][2] + 1);
			_countCardEpic = UnityEngine.Random.Range (RangeCountCardOpenForChestReward[KindCard.Epic][1], RangeCountCardOpenForChestReward[KindCard.Epic][2] + 1);
			_countCardLegendary = UnityEngine.Random.Range (RangeCountCardOpenForChestReward[KindCard.Legendary][1], RangeCountCardOpenForChestReward[KindCard.Legendary][2] + 1);
		}
		return ReturnIDKindOfChest (index, kindOfChestReward,
			_countCardCommon, _countCardRare, _countCardEpic, _countCardLegendary,
			RangeCountCardOpenForChestReward[KindCard.Common][0],
			RangeCountCardOpenForChestReward[KindCard.Rare][0],
			RangeCountCardOpenForChestReward[KindCard.Epic][0],
			RangeCountCardOpenForChestReward[KindCard.Legendary][0]
			);
	}

	private Dictionary<int, int> ReturnIDKindOfChest (int index, GameManager.KindOfChest kindOfChest, int countCommon, int countRare, int countEpic, int countLegend, int maxNumberCardCommon, int maxNumberCardRare, int maxNumberCardEpic, int maxNumberCardLegend)
	{
		switch (kindOfChest)
		{
			case GameManager.KindOfChest.Wood:
				return Instance.ChestRewardData.WoodChest (index, countCommon, countRare, countEpic, countLegend,
					maxNumberCardCommon, maxNumberCardRare, maxNumberCardEpic, maxNumberCardLegend);
			case GameManager.KindOfChest.Silver:
				return Instance.ChestRewardData.SilverChest (index, countCommon, countRare, countEpic, countLegend,
					maxNumberCardCommon, maxNumberCardRare, maxNumberCardEpic, maxNumberCardLegend);
			case GameManager.KindOfChest.Gold:
				return Instance.ChestRewardData.GoldChest (index, countCommon, countRare, countEpic, countLegend,
					maxNumberCardCommon, maxNumberCardRare, maxNumberCardEpic, maxNumberCardLegend);
		}
		return Instance.ChestRewardData.WoodChest (index, countCommon, countRare, countEpic, countLegend,
	maxNumberCardCommon, maxNumberCardRare, maxNumberCardEpic, maxNumberCardLegend);
	}

	public void LoadCardRewardDataShop (int index, GameManager.KindOfChest kindOfChestShop)
	{
		IdNumberCardEnemyChestInShop = new Dictionary<int, int> ();
		Dictionary<KindCard, List<int>> RangeCountCardOpenForChestInShop = GetRangeCountCardForChestInShop (index, kindOfChestShop);
		int _countCardCommon = UnityEngine.Random.Range (RangeCountCardOpenForChestInShop[KindCard.Common][1], RangeCountCardOpenForChestInShop[KindCard.Common][2] + 1);
		int _countCardRare = UnityEngine.Random.Range (RangeCountCardOpenForChestInShop[KindCard.Rare][1], RangeCountCardOpenForChestInShop[KindCard.Rare][2] + 1);
		int _countCardEpic = UnityEngine.Random.Range (RangeCountCardOpenForChestInShop[KindCard.Epic][1], RangeCountCardOpenForChestInShop[KindCard.Epic][2] + 1);
		int _countCardLegendary = UnityEngine.Random.Range (RangeCountCardOpenForChestInShop[KindCard.Legendary][1], RangeCountCardOpenForChestInShop[KindCard.Legendary][2] + 1);
		IdNumberCardEnemyChestInShop = ReturnIdNumberKindOfChestShop (index, kindOfChestShop,
			_countCardCommon, _countCardRare, _countCardEpic, _countCardLegendary,
			RangeCountCardOpenForChestInShop[KindCard.Common][0],
			RangeCountCardOpenForChestInShop[KindCard.Rare][0],
			RangeCountCardOpenForChestInShop[KindCard.Epic][0],
			RangeCountCardOpenForChestInShop[KindCard.Legendary][0]
			);
	}

	private Dictionary<global::GameManager.KindCard, List<int>> GetRangeCountCardForChestInShop (int index, GameManager.KindOfChest _kindChest)
	{
		ChestInShopData _data0 = DataChestInShop[index];
		List<int> _listNum_Range_DatasCommon = new List<int> ();
		List<int> _listNum_Range_DatasRare = new List<int> ();
		List<int> _listNum_Range_DatasEpic = new List<int> ();
		List<int> _listNum_Range_DatasLegend = new List<int> ();

		switch (_kindChest)
		{
			case GameManager.KindOfChest.Wood:
				_listNum_Range_DatasCommon = _data0.NumberCardCommon;
				_listNum_Range_DatasRare = _data0.NumberCardRare;
				_listNum_Range_DatasEpic = _data0.NumberCardEpic;
				_listNum_Range_DatasLegend = _data0.NumberCardLegend;

				break;
			case GameManager.KindOfChest.Silver:
				_listNum_Range_DatasCommon = _data0.NumberCardCommonChestRare;
				_listNum_Range_DatasRare = _data0.NumberCardRareChestRare;
				_listNum_Range_DatasEpic = _data0.NumberCardEpicChestRare;
				_listNum_Range_DatasLegend = _data0.NumberCardLegendChestRare;
				break;
			case GameManager.KindOfChest.Gold:
				_listNum_Range_DatasCommon = _data0.NumberCardCommonChestEpic;
				_listNum_Range_DatasRare = _data0.NumberCardRareChestEpic;
				_listNum_Range_DatasEpic = _data0.NumberCardEpicChestEpic;
				_listNum_Range_DatasLegend = _data0.NumberCardLegendChestEpic;
				break;
			case GameManager.KindOfChest.Magic:
			case GameManager.KindOfChest.SuperMagic:
				_listNum_Range_DatasCommon = _data0.NumberCardCommonChestLegend;
				_listNum_Range_DatasRare = _data0.NumberCardRareChestLegend;
				_listNum_Range_DatasEpic = _data0.NumberCardEpicChestLegend;
				_listNum_Range_DatasLegend = _data0.NumberCardLegendChestLegend;
				break;
		}
		Dictionary<global::GameManager.KindCard, List<int>> _tempDictionary = new Dictionary<global::GameManager.KindCard, List<int>> ();
		_tempDictionary.Add (KindCard.Common, _listNum_Range_DatasCommon);
		_tempDictionary.Add (KindCard.Rare, _listNum_Range_DatasRare);
		_tempDictionary.Add (KindCard.Epic, _listNum_Range_DatasEpic);
		_tempDictionary.Add (KindCard.Legendary, _listNum_Range_DatasLegend);
		return _tempDictionary;
	}

	public void LoadCoinWithChestInShop (int index, GameManager.KindOfChest kindOfChestShop)
	{
		CoinForChestShop = new List<int> ();
		switch (kindOfChestShop)
		{
			case GameManager.KindOfChest.Wood:
				// common
				CoinForChestShop.Clear ();
				CoinForChestShop.Add (Instance.DataCoinGemInChestShop[index].FreeChestCoin);
				break;
			case GameManager.KindOfChest.Silver:
				//rare
				CoinForChestShop.Clear ();
				CoinForChestShop.Add (Instance.DataCoinGemInChestShop[index].RareChestCoin);
				break;
			case GameManager.KindOfChest.Gold:
				//epic
				CoinForChestShop.Clear ();
				CoinForChestShop.Add (Instance.DataCoinGemInChestShop[index].EpicChestCoin);
				break;
			case GameManager.KindOfChest.Magic:
				//legend
				CoinForChestShop.Clear ();
				CoinForChestShop.Add (Instance.DataCoinGemInChestShop[index].LegendaryChestCoin);
				break;
			case GameManager.KindOfChest.SuperMagic:
				CoinForChestShop.Clear ();
				CoinForChestShop.Add (Instance.DataCoinGemInChestShop[index].LegendaryChestCoin);
				break;
			default:
				CoinForChestShop.Clear ();
				CoinForChestShop.Add (Instance.DataCoinGemInChestShop[index].FreeChestCoin);
				break;
		}
	}

	public void LoadGemWithChestInShop (int index, GameManager.KindOfChest kindOfChestShop)
	{
		GemsForChestShop = new List<int> ();
		switch (kindOfChestShop)
		{
			case GameManager.KindOfChest.Wood:
				// common
				GemsForChestShop.Clear ();
				GemsForChestShop.Add (Instance.DataCoinGemInChestShop[index].FreeChestGem);
				break;
			case GameManager.KindOfChest.Silver:
				//rare
				GemsForChestShop.Clear ();
				GemsForChestShop.Add (Instance.DataCoinGemInChestShop[index].RareChestGem);
				break;
			case GameManager.KindOfChest.Gold:
				//epic
				GemsForChestShop.Clear ();
				GemsForChestShop.Add (Instance.DataCoinGemInChestShop[index].EpicChestGem);
				break;
			case GameManager.KindOfChest.Magic:
				//legend
				GemsForChestShop.Clear ();
				GemsForChestShop.Add (Instance.DataCoinGemInChestShop[index].LegendaryChestGem);
				break;
			case GameManager.KindOfChest.SuperMagic:
				GemsForChestShop.Clear ();
				GemsForChestShop.Add (Instance.DataCoinGemInChestShop[index].LegendaryChestGem);
				break;
			default:
				GemsForChestShop.Clear ();
				GemsForChestShop.Add (Instance.DataCoinGemInChestShop[index].FreeChestGem);
				break;
		}
	}


	private Dictionary<int, int> ReturnIdNumberKindOfChestShop (int index, GameManager.KindOfChest kindOfChest, int countCommon, int countRare, int countEpic, int countLegend, int maxNumberCardCommon, int maxNumberCardRare, int maxNumberCardEpic, int maxNumberCardLegend)
	{
		switch (kindOfChest)
		{
			case GameManager.KindOfChest.Wood:
				return Instance.ChestShopData.WoodChest (index, countCommon, countRare, countEpic, countLegend,
					maxNumberCardCommon, maxNumberCardRare, maxNumberCardEpic, maxNumberCardLegend);
			case GameManager.KindOfChest.Silver:
				return Instance.ChestShopData.SilverChest (index, countCommon, countRare, countEpic, countLegend,
					maxNumberCardCommon, maxNumberCardRare, maxNumberCardEpic, maxNumberCardLegend);
			case GameManager.KindOfChest.Gold:
				return Instance.ChestShopData.GoldChest (index, countCommon, countRare, countEpic, countLegend,
					maxNumberCardCommon, maxNumberCardRare, maxNumberCardEpic, maxNumberCardLegend);
			case GameManager.KindOfChest.Magic:
				return Instance.ChestShopData.MagicChest (index, countCommon, countRare, countEpic, countLegend,
					maxNumberCardCommon, maxNumberCardRare, maxNumberCardEpic, maxNumberCardLegend);
		}
		return Instance.ChestShopData.WoodChest (index, countCommon, countRare, countEpic, countLegend,
			maxNumberCardCommon, maxNumberCardRare, maxNumberCardEpic, maxNumberCardLegend);
	}

	public string ConvertKindChestShop (KindOfChest kindChest)
	{
		switch (kindChest)
		{
			case KindOfChest.Wood:
				return "Wood";
			case KindOfChest.Silver:
				return "Silver";
			case KindOfChest.Gold:
				return "Gold";
			case KindOfChest.Magic:
				return "Magic";
			case KindOfChest.SuperMagic:
				return "SuperMagic";
		}
		return "";
	}



}
