﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class ChestShopController : MonoBehaviour
{
	public Dictionary<int, ChestInShopData> ChestData;

	public Dictionary<int, int> WoodChest (int index, int countCommon, int countRare, int countEpic, int countLegendary, int maxNumberCardCommon, int maxNumberCardRare, int maxNumberCardEpic, int maxNumberCardLegend)
	{
		int currentIndex = index;
		int commonWoodChestRate = (int) ChestData[currentIndex].FreeCommonRate;
		int rareWoodChestRate = (int) ChestData[currentIndex].FreeRareRate;
		int epicWoodChestRate = (int) ChestData[currentIndex].FreeEpicRate;
		int legendaryWoodChestRate = (int) ChestData[currentIndex].FreeLegendRate;

		return GenerateContaierData (index, countCommon, countRare,
			countEpic, countLegendary,
			maxNumberCardCommon, maxNumberCardRare,
			maxNumberCardEpic, maxNumberCardLegend,
			commonWoodChestRate, rareWoodChestRate,
			epicWoodChestRate, legendaryWoodChestRate
			);
	}
	public Dictionary<int, int> SilverChest (int index, int countCommon, int countRare, int countEpic, int countLegendary, int maxNumberCardCommon, int maxNumberCardRare, int maxNumberCardEpic, int maxNumberCardLegend)
	{
		int currentIndex = index;
		int commonSilverChestRate = (int) ChestData[currentIndex].BlueCommonRate;
		int rareSilverChestRate = (int) ChestData[currentIndex].BlueRareRate;
		int epicSilverChestRate = (int) ChestData[currentIndex].BlueEpicRate;
		int legendarySilverChestRate = (int) ChestData[currentIndex].BlueLegendRate;

		return GenerateContaierData (index, countCommon, countRare,
			countEpic, countLegendary,
			maxNumberCardCommon, maxNumberCardRare,
			maxNumberCardEpic, maxNumberCardLegend,
			commonSilverChestRate, rareSilverChestRate,
			epicSilverChestRate, legendarySilverChestRate);

	}
	public Dictionary<int, int> GoldChest (int index, int countCommon, int countRare, int countEpic, int countLegendary, int maxNumberCardCommon, int maxNumberCardRare, int maxNumberCardEpic, int maxNumberCardLegend)
	{
		int currentIndex = index;
		int commonGoldChestRate = (int) ChestData[currentIndex].VioletCommonRate;
		int rareGoldChestRate = (int) ChestData[currentIndex].VioletRareRate;
		int epicGoldChestRate = (int) ChestData[currentIndex].VioletEpicRate;
		int legendaryGoldChestRate = (int) ChestData[currentIndex].VioletLegendRate;

		return GenerateContaierData (index, countCommon, countRare,
		countEpic, countLegendary,
		maxNumberCardCommon, maxNumberCardRare,
		maxNumberCardEpic, maxNumberCardLegend,
		commonGoldChestRate, rareGoldChestRate,
		epicGoldChestRate, legendaryGoldChestRate);
	}
	public Dictionary<int, int> MagicChest (int index, int countCommon, int countRare, int countEpic, int countLegendary, int maxNumberCardCommon, int maxNumberCardRare, int maxNumberCardEpic, int maxNumberCardLegend)
	{
		int currentIndex = index;
		int commonMagicChestRate = (int) ChestData[currentIndex].RedCommonRate;
		int rareMagicChestRate = (int) ChestData[currentIndex].RedRareRate;
		int epicMagicChestRate = (int) ChestData[currentIndex].RedEpicRate;
		int legendaryMagicChestRate = (int) ChestData[currentIndex].RedLegendRate;

		return GenerateContaierData (index, countCommon, countRare,
			countEpic, countLegendary,
			maxNumberCardCommon, maxNumberCardRare,
			maxNumberCardEpic, maxNumberCardLegend,
			commonMagicChestRate, rareMagicChestRate,
			epicMagicChestRate, legendaryMagicChestRate);
	}

	private List<int> ReturnCommonEnemyId (int index, int _count)
	{
		List<int> commons = ChestData[index].CommonCards;
		List<int> _containerCommon = new List<int> ();
		if (_count > commons.Count)
		{
			return UltiGenerics.Shuffle (commons);
		}
		else
		{
			for (int j = 0; j < _count; j++)
			{
				int tempId = commons[Random.Range (0, commons.Count)];
				int _tempId;

				if (UltiGenerics.ExistIdInList (tempId, _containerCommon))
				{
					do
					{
						List<int> _tempo = UltiGenerics.Except (commons, _containerCommon);
						_tempId = _tempo[Random.Range (0, _tempo.Count)];
					} while (UltiGenerics.ExistIdInList (_tempId, _containerCommon));

					tempId = _tempId;
					_containerCommon.Add (_tempId);
				}
				else
				{
					_containerCommon.Add (tempId);
				}
			}

			return _containerCommon;
		}
	}
	private List<int> ReturnRareEnemyId (int index, int _count)
	{
		List<int> rares = ChestData[index].RareCards;
		List<int> _containerRare = new List<int> ();
		if (_count > rares.Count)
		{
			return UltiGenerics.Shuffle (rares);
		}
		else
		{
			for (int j = 0; j < _count; j++)
			{
				int tempId = rares[Random.Range (0, rares.Count)];
				int _tempId;

				if (UltiGenerics.ExistIdInList (tempId, _containerRare))
				{
					do
					{
						List<int> _tempo = UltiGenerics.Except (rares, _containerRare);
						_tempId = _tempo[Random.Range (0, _tempo.Count)];
					} while (UltiGenerics.ExistIdInList (_tempId, _containerRare));

					tempId = _tempId;
					_containerRare.Add (_tempId);
				}
				else
				{
					_containerRare.Add (tempId);
				}
			}

			return _containerRare;
		}
	}
	private List<int> ReturnEpicEnemyId (int index, int _count)
	{
		List<int> epics = ChestData[index].EpicCards;
		List<int> _containerEpic = new List<int> ();
		if (_count > epics.Count)
		{
			return UltiGenerics.Shuffle (epics);
		}
		else
		{
			for (int j = 0; j < _count; j++)
			{
				int tempId = epics[Random.Range (0, epics.Count)];
				int _tempId;

				if (UltiGenerics.ExistIdInList (tempId, _containerEpic))
				{
					do
					{
						List<int> _tempo = UltiGenerics.Except (epics, _containerEpic);
						_tempId = _tempo[Random.Range (0, _tempo.Count)];
					} while (UltiGenerics.ExistIdInList (_tempId, _containerEpic));

					tempId = _tempId;
					_containerEpic.Add (_tempId);
				}
				else
				{
					_containerEpic.Add (tempId);
				}
			}

			return _containerEpic;
		}
	}
	private List<int> ReturnLegendaryEnemyId (int index, int _count)
	{
		List<int> legends = ChestData[index].LegendCards;
		List<int> _containerLegend = new List<int> ();
		if (_count > legends.Count)
		{
			return UltiGenerics.Shuffle (legends);
		}
		else
		{
			for (int j = 0; j < _count; j++)
			{
				int tempId = legends[Random.Range (0, legends.Count)];
				int _tempId;

				if (UltiGenerics.ExistIdInList (tempId, _containerLegend))
				{
					do
					{
						List<int> _tempo = UltiGenerics.Except (legends, _containerLegend);
						_tempId = _tempo[Random.Range (0, _tempo.Count)];
					} while (UltiGenerics.ExistIdInList (_tempId, _containerLegend));

					tempId = _tempId;
					_containerLegend.Add (_tempId);
				}
				else
				{
					_containerLegend.Add (tempId);
				}
			}

			return _containerLegend;
		}
	}

	private Dictionary<int, int> GenerateContaierData (int index, int countCommon,
		int countRare, int countEpic, int countLegendary, int maxNumberCardCommon,
		int maxNumberCardRare, int maxNumberCardEpic, int maxNumberCardLegend,
		int commontRate, int rareRate, int epicRate, int legendaryRate)
	{
		List<int> container = new List<int> ();
		List<int> containerNumberCard = new List<int> ();

		#region common
		if (countCommon == 0 || commontRate == 0)
		{

		}
		else
		if (commontRate == 100)
		{
			CaculateListIdEnemy (index, countCommon, maxNumberCardCommon, container, containerNumberCard, GameManager.KindCard.Common);
		}
		else
		{
			if (UltiGenerics.RandomlizeMax (0, 100) <= commontRate)
			{
				CaculateListIdEnemy (index, countCommon, maxNumberCardCommon, container, containerNumberCard, GameManager.KindCard.Common);
			}
		}
		#endregion

		#region rare 
		if (countRare == 0 || rareRate == 0)
		{

		}
		else if (rareRate == 100)
		{
			CaculateListIdEnemy (index, countRare, maxNumberCardRare, container, containerNumberCard, GameManager.KindCard.Rare);
		}
		else
		{
			if (UltiGenerics.RandomlizeMax (0, 100) <= rareRate)
			{
				CaculateListIdEnemy (index, countRare, maxNumberCardRare, container, containerNumberCard, GameManager.KindCard.Rare);
			}
		}
		#endregion

		#region epic 
		if (countEpic == 0 || epicRate == 0)
		{

		}
		else if (epicRate == 100)
		{
			CaculateListIdEnemy (index, countEpic, maxNumberCardEpic, container, containerNumberCard, GameManager.KindCard.Epic);
		}
		else
		{
			if (UltiGenerics.RandomlizeMax (0, 100) <= epicRate)
			{
				CaculateListIdEnemy (index, countEpic, maxNumberCardEpic, container, containerNumberCard, GameManager.KindCard.Epic);
			}
		}
		#endregion

		#region legend 
		if (countLegendary == 0 || legendaryRate == 0)
		{

		}
		else if (legendaryRate == 100)
		{
			CaculateListIdEnemy (index, countLegendary, maxNumberCardLegend, container, containerNumberCard, GameManager.KindCard.Legendary);
		}
		else
		{
			if (UltiGenerics.RandomlizeMax (0, 100) <= legendaryRate)
			{
				CaculateListIdEnemy (index, countLegendary, maxNumberCardLegend, container, containerNumberCard, GameManager.KindCard.Legendary);
			}
		}
		#endregion


		return UltiGenerics.MakeDictionary (container, containerNumberCard);
	}

	private void CaculateListIdEnemy (int index, int countCard,
		int maxNumberCard,
		List<int> container,
		List<int> containerNumberCard, GameManager.KindCard kindOfCard)
	{
		List<int> tempContainerID = new List<int> ();
		List<int> tempContainerNumberCard = new List<int> ();

		// init base data
		switch (kindOfCard)
		{
			case GameManager.KindCard.Common:
				tempContainerID = ReturnCommonEnemyId (index, countCard);
				break;
			case GameManager.KindCard.Rare:
				tempContainerID = ReturnRareEnemyId (index, countCard);
				break;
			case GameManager.KindCard.Epic:
				tempContainerID = ReturnEpicEnemyId (index, countCard);
				break;
			case GameManager.KindCard.Legendary:
				tempContainerID = ReturnLegendaryEnemyId (index, countCard);
				break;
			default:
				tempContainerID = ReturnCommonEnemyId (index, countCard);
				break;
		}


		tempContainerNumberCard = GameManager.Instance.GetNumberCardInCardCollection (tempContainerID);

		//create dictionary sort id acsending follow NumberCard
		Dictionary<int, int> dictionary = UltiGenerics.MakeDictionary (tempContainerID, tempContainerNumberCard)
			.OrderBy (fu => fu.Value).ToDictionary (t => t.Key, t => t.Value);

		//get list id before sort
		tempContainerID = UltiGenerics.GetKeysDictionary (dictionary);
		tempContainerNumberCard = UltiGenerics.CaculateNumberCardFollowRate (countCard,
			maxNumberCard, UltiGenerics.RateWithCount (countCard, 100, GameManager.Instance.MinValueRandomForAlgorithmChest));

		//shuffle
		dictionary = UltiGenerics.Shuffle (UltiGenerics.MakeDictionary (tempContainerID, tempContainerNumberCard));
		tempContainerID = UltiGenerics.GetKeysDictionary (dictionary);
		tempContainerNumberCard = UltiGenerics.GetValuesDictionary (dictionary);
		container.AddRange (tempContainerID);
		containerNumberCard.AddRange (tempContainerNumberCard);
	}

}

