﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using UnityEngine.UI;

public class CardInShopController : MonoBehaviour
{
	[HideInInspector]
	public Dictionary<int, CardInShopData> CardData;
	[HideInInspector]
	public DataSellCardInShop DataSellCard;
	[HideInInspector]
	public DataCostOfCard DataCostCard;

	public Dictionary<int, DetailtDataCardInShop> CaculateDataCardSellInShop (int index, int countCommon, int countRare, int countEpic, int countLegendary)
	{
		int currentIndex = index;
		int commontRatio = (int) CardData[currentIndex].CommonRate;
		int rareRatio = (int) CardData[currentIndex].RareRate;
		int epicRatio = (int) CardData[currentIndex].EpicRate;
		int legendaryRatio = (int) CardData[currentIndex].LegendRate;

		return GenerateContaierData (countCommon, countRare,
			countEpic, countLegendary,
			commontRatio, rareRatio,
			epicRatio, legendaryRatio);
	}

	private Dictionary<int, DetailtDataCardInShop> GenerateContaierData (int countCommon,
	int countRare, int countEpic, int countLegendary,
	int commontRate, int rareRate, int epicRate, int legendaryRate)
	{
		List<DetailtDataCardInShop> containerDetailCard = new List<DetailtDataCardInShop> ();

		#region common
		if (countCommon == 0 || commontRate == 0)
		{

		}
		else if (commontRate == 100)
		{
			CaculateNewListIdEnemy (countCommon, containerDetailCard, GameManager.KindCard.Common, DataCostCard.CostOfKindCard);
		}
		else
		{
			if (UltiGenerics.RandomlizeMax (0, 100) <= commontRate)
			{
				CaculateNewListIdEnemy (countCommon, containerDetailCard, GameManager.KindCard.Common, DataCostCard.CostOfKindCard);
			}
		}
		#endregion

		#region rare 
		if (countRare == 0 || rareRate == 0)
		{

		}
		else if (rareRate == 100)
		{
			countEpic = 1;
			CaculateNewListIdEnemy (countRare, containerDetailCard, GameManager.KindCard.Rare, DataCostCard.CostOfKindCard);
		}
		else
		{
			if (UltiGenerics.RandomlizeMax (0, 100) <= rareRate)
			{
				countEpic = 1;
				CaculateNewListIdEnemy (countRare, containerDetailCard, GameManager.KindCard.Rare, DataCostCard.CostOfKindCard);
			}
			else if (containerDetailCard.Count < 3)
			{
				countRare = (int) UltiGenerics.RandomlizeMax (1, 3);
				CaculateNewListIdEnemy (countRare, containerDetailCard, GameManager.KindCard.Rare, DataCostCard.CostOfKindCard);
			}
		}
		#endregion

		#region epic 
		if (countEpic == 0 || epicRate == 0 || containerDetailCard.Count >= 3)
		{

		}
		else if (epicRate == 100)
		{
			countEpic = 1;
			CaculateNewListIdEnemy (countEpic, containerDetailCard, GameManager.KindCard.Epic, DataCostCard.CostOfKindCard);
		}
		else
		{
			if (UltiGenerics.RandomlizeMax (0, 100) <= epicRate)
			{
				countEpic = 1;
				CaculateNewListIdEnemy (countEpic, containerDetailCard, GameManager.KindCard.Epic, DataCostCard.CostOfKindCard);
			}
			else if (containerDetailCard.Count < 3)
			{
				countEpic = 1;
				CaculateNewListIdEnemy (countEpic, containerDetailCard, GameManager.KindCard.Epic, DataCostCard.CostOfKindCard);
			}
		}
		#endregion

		#region legend 
		if (countLegendary == 0 || legendaryRate == 0)
		{

		}
		else if (legendaryRate == 100)
		{
			CaculateNewListIdEnemy (countLegendary, containerDetailCard, GameManager.KindCard.Legendary, DataCostCard.CostOfKindCard);
		}
		else
		{
			if (UltiGenerics.RandomlizeMax (0, 100) <= legendaryRate)
			{
				CaculateNewListIdEnemy (countLegendary, containerDetailCard, GameManager.KindCard.Legendary, DataCostCard.CostOfKindCard);
			}
		}
		#endregion

		return UltiGenerics.MakeDictionary (new List<int> { 0, 1, 2 }, containerDetailCard);
	}

	private void CaculateNewListIdEnemy (int countCard, List<DetailtDataCardInShop> containerDetailtCard, GameManager.KindCard kindOfCard, Dictionary<GameManager.KindCard, List<int>> dictionaryCost)
	{
		List<int> tempContainerID = new List<int> ();
		List<DetailtDataCardInShop> tempContainerDetailCard = new List<DetailtDataCardInShop> ();

		// init base data
		switch (kindOfCard)
		{
			case GameManager.KindCard.Common:
				tempContainerID = ReturnCommonEnemyId (1, countCard);
				foreach (var item in tempContainerID)
				{
					tempContainerDetailCard.Add (new DetailtDataCardInShop (GameManager.KindCard.Common, item, dictionaryCost[GameManager.KindCard.Common][0], false, dictionaryCost[GameManager.KindCard.Common].Count, 0));
				}
				break;
			case GameManager.KindCard.Rare:
				tempContainerID = ReturnRareEnemyId (1, countCard);
				foreach (var item in tempContainerID)
				{
					tempContainerDetailCard.Add (new DetailtDataCardInShop (GameManager.KindCard.Rare, item, dictionaryCost[GameManager.KindCard.Rare][0], false, dictionaryCost[GameManager.KindCard.Rare].Count, 0));
				}
				break;
			case GameManager.KindCard.Epic:
				tempContainerID = ReturnEpicEnemyId (1, countCard);
				foreach (var item in tempContainerID)
				{
					tempContainerDetailCard.Add (new DetailtDataCardInShop (GameManager.KindCard.Epic, item, dictionaryCost[GameManager.KindCard.Epic][0], false, dictionaryCost[GameManager.KindCard.Epic].Count, 0));
				}
				break;
			case GameManager.KindCard.Legendary:
				tempContainerID = ReturnLegendaryEnemyId (1, countCard);
				foreach (var item in tempContainerID)
				{
					tempContainerDetailCard.Add (new DetailtDataCardInShop (GameManager.KindCard.Legendary, item, dictionaryCost[GameManager.KindCard.Legendary][0], false, dictionaryCost[GameManager.KindCard.Legendary].Count, 0));
				}
				break;
			default:
				tempContainerID = ReturnCommonEnemyId (1, countCard);
				foreach (var item in tempContainerID)
				{
					tempContainerDetailCard.Add (new DetailtDataCardInShop (GameManager.KindCard.Common, item, dictionaryCost[GameManager.KindCard.Common][0], false, dictionaryCost[GameManager.KindCard.Common].Count, 0));
				}
				break;
		}
		containerDetailtCard.AddRange (tempContainerDetailCard);
	}


	#region return Id

	private List<int> ReturnCommonEnemyId (int index,int _count)
	{
		List<int> commons = CardData[index].CommonCards;
		List<int> _containerCommon = new List<int> ();
		if (_count > commons.Count)
		{
			return UltiGenerics.Shuffle (commons);
		}
		else
		{
			for (int j = 0; j < _count; j++)
			{
				int tempId = commons[RandomNumber.GenerateRandom (0, commons.Count)];
				int _tempId;

				if (UltiGenerics.ExistIdInList (tempId, _containerCommon))
				{
					do
					{
						List<int> _tempo = UltiGenerics.Except (commons, _containerCommon);
						_tempId = _tempo[RandomNumber.GenerateRandom (0, _tempo.Count)];
					} while (UltiGenerics.ExistIdInList (_tempId, _containerCommon));

					tempId = _tempId;
					_containerCommon.Add (_tempId);
				}
				else
				{
					_containerCommon.Add (tempId);
				}
			}

			return _containerCommon;
		}
	}
	private List<int> ReturnRareEnemyId (int index, int _count)
	{
		List<int> rares = CardData[index].RareCards;
		List<int> _containerRare = new List<int> ();
		if (_count > rares.Count)
		{
			return UltiGenerics.Shuffle (rares);
		}
		else
		{
			for (int j = 0; j < _count; j++)
			{
				int tempId = rares[RandomNumber.GenerateRandom (0, rares.Count)];
				int _tempId;

				if (UltiGenerics.ExistIdInList (tempId, _containerRare))
				{
					do
					{
						List<int> _tempo = UltiGenerics.Except (rares, _containerRare);
						_tempId = _tempo[RandomNumber.GenerateRandom (0, _tempo.Count)];
					} while (UltiGenerics.ExistIdInList (_tempId, _containerRare));

					tempId = _tempId;
					_containerRare.Add (_tempId);
				}
				else
				{
					_containerRare.Add (tempId);
				}
			}

			return _containerRare;
		}
	}
	private List<int> ReturnEpicEnemyId (int index, int _count)
	{
		List<int> epics = CardData[index].EpicCards;
		List<int> _containerEpic = new List<int> ();
		if (_count > epics.Count)
		{
			return UltiGenerics.Shuffle (epics);
		}
		else
		{
			for (int j = 0; j < _count; j++)
			{
				int tempId = epics[RandomNumber.GenerateRandom (0, epics.Count)];
				int _tempId;

				if (UltiGenerics.ExistIdInList (tempId, _containerEpic))
				{
					do
					{
						List<int> _tempo = UltiGenerics.Except (epics, _containerEpic);
						_tempId = _tempo[RandomNumber.GenerateRandom (0, _tempo.Count)];
					} while (UltiGenerics.ExistIdInList (_tempId, _containerEpic));

					tempId = _tempId;
					_containerEpic.Add (_tempId);
				}
				else
				{
					_containerEpic.Add (tempId);
				}
			}

			return _containerEpic;
		}
	}
	private List<int> ReturnLegendaryEnemyId (int index, int _count)
	{
		List<int> legends = CardData[index].LegendCards;
		List<int> _containerLegend = new List<int> ();
		if (_count > legends.Count)
		{
			return UltiGenerics.Shuffle (legends);
		}
		else
		{
			for (int j = 0; j < _count; j++)
			{
				int tempId = legends[RandomNumber.GenerateRandom (0, legends.Count)];
				int _tempId;

				if (UltiGenerics.ExistIdInList (tempId, _containerLegend))
				{
					do
					{
						List<int> _tempo = UltiGenerics.Except (legends, _containerLegend);
						_tempId = _tempo[RandomNumber.GenerateRandom (0, _tempo.Count)];
					} while (UltiGenerics.ExistIdInList (_tempId, _containerLegend));

					tempId = _tempId;
					_containerLegend.Add (_tempId);
				}
				else
				{
					_containerLegend.Add (tempId);
				}
			}

			return _containerLegend;
		}
	}

	#endregion


	public void InitDataCardInShop (bool isReciveoldData = false)
	{
		if (!isReciveoldData)
		{
			DataCostCard.CostOfKindCard = GetCoinData (1);
			DataSellCard.IdCostDictionary = CaculateDataCardSellInShop (1,1, 2, 1, 0);
			SaveDataSellCard ();
			SaveDataCostCard ();
		}
		else
		{
			ReciveOldDataSellCard ();
			ReciveOldDataCostCard ();
		}
	}

	/// <summary>
	/// 
	/// </summary>
	/// <param name="nameFile"> nameFile = "/name.lu" </param>
	/// <returns></returns>
	public bool CheckExistFile (string nameFile)
	{
		return File.Exists (Application.persistentDataPath + nameFile);
	}

	public void ClearData ()
	{
		if (CheckExistFile ("/aprius.lu"))
		{
			try
			{
				File.Delete (Application.persistentDataPath + "/aprius.lu");
			}
			catch (IOException e)
			{
#if UNITY_EDITOR
				print (e.Message);
#endif
				return;
			}
		}

		if (CheckExistFile ("/andromeda.lu"))
		{
			try
			{
				File.Delete (Application.persistentDataPath + "/andromeda.lu");
			}
			catch (System.IO.IOException e)
			{
#if UNITY_EDITOR
				print (e.Message);
#endif
				return;
			}
		}
	}


	#region SaveLoad
	public void SaveDataSellCard ()
	{
		BinaryFormatter bf = new BinaryFormatter ();
		FileStream stream = new FileStream (Application.persistentDataPath + "/aprius.lu", FileMode.Create);

		bf.Serialize (stream, DataSellCard);
		stream.Close ();
	}
	public void SaveDataCostCard ()
	{
		BinaryFormatter bf = new BinaryFormatter ();
		FileStream stream = new FileStream (Application.persistentDataPath + "/andromeda.lu", FileMode.Create);

		bf.Serialize (stream, DataCostCard);
		stream.Close ();
	}

	public void ReciveOldDataSellCard ()
	{
		if (File.Exists (Application.persistentDataPath + "/aprius.lu"))
		{
			BinaryFormatter bf = new BinaryFormatter ();
			FileStream stream = new FileStream (Application.persistentDataPath + "/aprius.lu", FileMode.Open);

			DataSellCardInShop data = bf.Deserialize (stream) as DataSellCardInShop;
			stream.Close ();
			DataSellCard.IdCostDictionary = data.IdCostDictionary;
		}
	}

	public void ReciveOldDataCostCard ()
	{
		if (File.Exists (Application.persistentDataPath + "/andromeda.lu"))
		{
			BinaryFormatter bf = new BinaryFormatter ();
			FileStream stream = new FileStream (Application.persistentDataPath + "/andromeda.lu", FileMode.Open);

			DataCostOfCard data = bf.Deserialize (stream) as DataCostOfCard;
			stream.Close ();
			DataCostCard.CostOfKindCard = data.CostOfKindCard;
		}
	}

	#endregion


	private Dictionary<GameManager.KindCard, List<int>> GetCoinData (int index)
	{
		int currentIndex = index;
		Dictionary<GameManager.KindCard, List<int>> tempo = new Dictionary<GameManager.KindCard, List<int>> ();
		tempo.Add (GameManager.KindCard.Common, CardData[currentIndex].CostCommons);
		tempo.Add (GameManager.KindCard.Rare, CardData[currentIndex].CostRares);
		tempo.Add (GameManager.KindCard.Epic, CardData[currentIndex].CostEpics);
		tempo.Add (GameManager.KindCard.Legendary, CardData[currentIndex].CostLegendaris);

		return tempo;
	}

	public GameManager.KindCard Convert (string typeCard)
	{
		switch (typeCard.ToLower ())
		{
			case "common":
				return GameManager.KindCard.Common;
			case "rare":
				return GameManager.KindCard.Rare;
			case "epic":
				return GameManager.KindCard.Epic;
			case "legendary":
				return GameManager.KindCard.Legendary;
			default:

				break;
		}
		return GameManager.KindCard.Common;
	}

	public string ReverseConvert (GameManager.KindCard typeCard)
	{
		switch (typeCard)
		{
			case GameManager.KindCard.Common:
				return "common";

			case GameManager.KindCard.Rare:
				return "rare";

			case GameManager.KindCard.Epic:
				return "epic";

			case GameManager.KindCard.Legendary:
				return "legend";
			default:
				break;
		}
		return "";
	}



	#region SerializableData

	[Serializable]
	public class DataSellCardInShop
	{
		public Dictionary<int, DetailtDataCardInShop> IdCostDictionary;
	}

	[Serializable]
	public class DataCostOfCard
	{
		public Dictionary<GameManager.KindCard, List<int>> CostOfKindCard;
	}

	[Serializable]
	public class DetailtDataCardInShop
	{
		public GameManager.KindCard kindCard;
		public int idCard;
		public int currentCost;
		public bool isSoldOut;
		public int maxCountCanBuy;
		public int currrentIndexOfCost;

		public DetailtDataCardInShop (GameManager.KindCard kindCard, int idCard, int currentCost, bool isSoldOut, int maxCountCanBuy, int currnetIndexOfCost)
		{
			this.kindCard = kindCard;
			this.idCard = idCard;
			this.currentCost = currentCost;
			this.isSoldOut = isSoldOut;
			this.maxCountCanBuy = maxCountCanBuy;
			this.currrentIndexOfCost = currnetIndexOfCost;
		}
	}

	#endregion

}

