﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardInShopData
{

	private List<int> rangeLevel;

	private List<int> commonCards;
	private List<int> rareCards;
	private List<int> epicCards;
	private List<int> legendCards;

	//free
	private int commonRate;
	private int rareRate;
	private int epicRate;
	private int legendRate;

	private List<int> costCommons;
	private List<int> costRares;
	private List<int> costEpics;
	private List<int> costLegendaris;

	public List<int> RangeLevel
	{
		get {
			return rangeLevel;
		}

		set {
			rangeLevel = value;
		}
	}

	public List<int> CommonCards
	{
		get {
			return commonCards;
		}

		set {
			commonCards = value;
		}
	}

	public List<int> RareCards
	{
		get {
			return rareCards;
		}

		set {
			rareCards = value;
		}
	}

	public List<int> EpicCards
	{
		get {
			return epicCards;
		}

		set {
			epicCards = value;
		}
	}

	public List<int> LegendCards
	{
		get {
			return legendCards;
		}

		set {
			legendCards = value;
		}
	}

	public int CommonRate
	{
		get {
			return commonRate;
		}

		set {
			commonRate = value;
		}
	}

	public int RareRate
	{
		get {
			return rareRate;
		}

		set {
			rareRate = value;
		}
	}

	public int EpicRate
	{
		get {
			return epicRate;
		}

		set {
			epicRate = value;
		}
	}

	public int LegendRate
	{
		get {
			return legendRate;
		}

		set {
			legendRate = value;
		}
	}

	public List<int> CostCommons
	{
		get {
			return costCommons;
		}

		set {
			costCommons = value;
		}
	}

	public List<int> CostRares
	{
		get {
			return costRares;
		}

		set {
			costRares = value;
		}
	}

	public List<int> CostEpics
	{
		get {
			return costEpics;
		}

		set {
			costEpics = value;
		}
	}

	public List<int> CostLegendaris
	{
		get {
			return costLegendaris;
		}

		set {
			costLegendaris = value;
		}
	}

	public CardInShopData (List<int> _rangeLevel,

	 List<int> _commonCards,
	 List<int> _rareCards,
	 List<int> _epicCards,
	 List<int> _legendCards,

	 int _commonRate,
	 int _rareRate,
	 int _epicRate,
	 int _legendRate,

	 List<int> _costCommons,
	 List<int> _costRares,
	 List<int> _costEpics,
	 List<int> _costLegendaris)
	{
		rangeLevel = _rangeLevel;

		commonCards = _commonCards;
		rareCards = _rareCards;
		epicCards = _epicCards;
		legendCards = _legendCards;

		commonRate = _commonRate;
		rareRate = _rareRate;
		epicRate = _epicRate;
		legendRate = _legendRate;

		costCommons = _costCommons;
		costRares = _costRares;
		costEpics = _costEpics;
		costLegendaris = _costLegendaris;
	}

}
