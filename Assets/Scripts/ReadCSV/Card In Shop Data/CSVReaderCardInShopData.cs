﻿using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;

public class CSVReaderCardInShopData : MonoBehaviour {


	public readonly static string SPLIT_RE = ",";
	public readonly static string SPLIT_AT = "&";
	public readonly static string LINE_SPLIT_RE = @"\n";

	public static Dictionary<int, CardInShopData> Read (TextAsset data)
	{
		var list = new Dictionary<int, CardInShopData> ();
		var lines = Regex.Split (data.text, LINE_SPLIT_RE);
		int length = lines.Length;
		if (length == 1)
			return list;
		for (int i = 1; i < lines.Length; i++)
		{
			list.Add (i, GetChestData (lines[i]));
		}
		return list;
	}

	public static Dictionary<int, CardInShopData> Read (string file)
	{
		TextAsset data = Resources.Load (file) as TextAsset;
		var list = Read (data);
		return list;
	}

	private static CardInShopData GetChestData (string input)
	{
		var lines = Regex.Split (input, SPLIT_RE);
		List<int> rangeLevel = new List<int> (Getdata (lines[0]));
		List<int> commonCards = new List<int> (Getdata (lines[1]));
		List<int> rareCards = new List<int> (Getdata (lines[2]));
		List<int> epicCards = new List<int> (Getdata (lines[3]));
		List<int> legendCards = new List<int> (Getdata (lines[4]));
		int freeCommonRate;
		int.TryParse (lines[5], out freeCommonRate);
		int freeRareRate;
		int.TryParse (lines[6], out freeRareRate);
		int freeEpicRate;
		int.TryParse (lines[7], out freeEpicRate);
		int freeLegendRate;
		int.TryParse (lines[8], out freeLegendRate);

		List<int> costCardCommon = new List<int> (Getdata (lines[9]));
		List<int> costCardRare = new List<int> (Getdata (lines[10]));
		List<int> costCardEpic = new List<int> (Getdata (lines[11]));
		List<int> costCardLegend = new List<int> (Getdata (lines[12]));

		return new CardInShopData (rangeLevel,
			commonCards,
			rareCards,
			epicCards,
			legendCards,
			freeCommonRate,
			freeRareRate,
			freeEpicRate,
			freeLegendRate,

			costCardCommon,
			costCardRare,
			costCardEpic,
			costCardLegend
			);
	}

	private static List<int> Getdata (string input)
	{
		var list = new List<int> ();
		input = input.Trim ();
		if (input.Length == 0)
		{
			return list;
		}
		var lines = Regex.Split (input, SPLIT_AT);
		for (int i = 0; i < lines.Length; i++)
		{
			int temp;
			int.TryParse (lines[i], out temp);
			list.Add (temp);
		}
		return list;
	}
}
