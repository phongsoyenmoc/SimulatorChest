﻿using System.Collections;
using System.Collections.Generic;
[System.Serializable]
public class ChestData
{
	public List<int> RangeLevel;
	public List<int> CommonCards;
	public List<int> RareCards;
	public List<int> EpicCards;
	public List<int> LegendaryCards;

	//wood
	public float CommonWoodChestRatio;
	public float RareWoodChestRatio;
	public float EpicWoodChestRatio;
	public float LegendaryWoodChestRatio;

	public List<int> NumberCardCommonChestWood;
	public List<int> NumberCardRareChestWood;
	public List<int> NumberCardEpicChestWood;
	public List<int> NumberCardLegendChestWood;

	//silver
	public float CommonSilverChestRatio;
	public float RareSilverChestRatio;
	public float EpicSilverChestRatio;
	public float LegendarySilverChestRatio;

	public List<int> NumberCardCommonChestSilver;
	public List<int> NumberCardRareChestSilver;
	public List<int> NumberCardEpicChestSilver;
	public List<int> NumberCardLegendChestSilver;

	//gold
	public float CommonGoldChestRatio;
	public float RareGoldChestRatio;
	public float EpicGoldChestRatio;
	public float LegendaryGoldChestRatio;

	public List<int> NumberCardCommonChestGold;
	public List<int> NumberCardRareChestGold;
	public List<int> NumberCardEpicChestGold;
	public List<int> NumberCardLegendChestGold;


	public ChestData (List<int> rangeLevel,
		List<int> commonCard,
		List<int> rareCard,
		List<int> epicCard,
		List<int> legendaryCard,
		float commonWoodChestRatio,
		float rareWoodChestRatio,
		float epicWoodChestRatio,
		float legendaryWoodChestRatio,

		List<int> numberCardCommonWood,
		List<int> numberCardRareWood,
		List<int> numberCardEpicWood,
		List<int> numberCardLegendWood,

		float commonSilverChestRatio,
		float rareSilverChestRatio,
		float epicSilverChestRatio,
		float legendarySilverChestRatio,

		List<int> numberCardCommonChestSilver,
		List<int> numberCardRareChestSilver,
		List<int> numberCardEpicChestSilver,
		List<int> numberCardLegendChestSilver,

		float commonGoldChestRatio,
		float rareGoldChestRatio,
		float epicGoldChestRatio,
		float legendaryGoldChestRatio,

		List<int> numberCardCommonChestGold,
		List<int> numberCardRareChestGold,
		List<int> numberCardEpicChestGold,
		List<int> numberCardLegendChestGold)
	{
		this.RangeLevel = rangeLevel;
		this.CommonCards = new List<int> (commonCard);
		this.RareCards = new List<int> (rareCard);
		this.EpicCards = new List<int> (epicCard);
		this.LegendaryCards = new List<int> (legendaryCard);
		this.CommonWoodChestRatio = commonWoodChestRatio;
		this.RareWoodChestRatio = rareWoodChestRatio;
		this.EpicWoodChestRatio = epicWoodChestRatio;
		this.LegendaryWoodChestRatio = legendaryWoodChestRatio;

		this.NumberCardCommonChestWood = numberCardCommonWood;
		this.NumberCardRareChestWood = numberCardRareWood;
		this.NumberCardEpicChestWood = numberCardEpicWood;
		this.NumberCardLegendChestWood = numberCardLegendWood;

		this.CommonSilverChestRatio = commonSilverChestRatio;
		this.RareSilverChestRatio = rareSilverChestRatio;
		this.EpicSilverChestRatio = epicSilverChestRatio;
		this.LegendarySilverChestRatio = legendarySilverChestRatio;

		this.NumberCardCommonChestSilver = numberCardCommonChestSilver;
		this.NumberCardRareChestSilver = numberCardRareChestSilver;
		this.NumberCardEpicChestSilver = numberCardEpicChestSilver;
		this.NumberCardLegendChestSilver = numberCardLegendChestSilver;

		this.CommonGoldChestRatio = commonGoldChestRatio;
		this.RareGoldChestRatio = rareGoldChestRatio;
		this.EpicGoldChestRatio = epicGoldChestRatio;
		this.LegendaryGoldChestRatio = legendaryGoldChestRatio;

		this.NumberCardCommonChestGold = numberCardCommonChestGold;
		this.NumberCardRareChestGold = numberCardRareChestGold;
		this.NumberCardEpicChestGold = numberCardEpicChestGold;
		this.NumberCardLegendChestGold = numberCardLegendChestGold;
	}
}
