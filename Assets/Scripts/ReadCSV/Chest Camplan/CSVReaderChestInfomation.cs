﻿using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;
public class CSVReaderChestInfomation
{
	public readonly static string SPLIT_RE = ",";
	public readonly static string SPLIT_AT = "&";
	public readonly static string LINE_SPLIT_RE = @"\n";

	public static Dictionary<int, ChestData> Read (TextAsset data)
	{
		var list = new Dictionary<int, ChestData> ();
		var lines = Regex.Split (data.text, LINE_SPLIT_RE);
		int length = lines.Length;
		if (length == 1)
			return list;
		for (int i = 1; i < lines.Length; i++)
		{
			list.Add (i, GetChestData (lines[i]));
		}
		return list;
	}

	public static Dictionary<int, ChestData> Read (string file)
	{
		TextAsset data = Resources.Load (file) as TextAsset;
		var list = Read (data);
		return list;
	}

	private static ChestData GetChestData (string input)
	{
		var lines = Regex.Split (input, SPLIT_RE);
		List<int> rangeLevel = new List<int> (Getdata (lines[0]));
		List<int> commonCard = new List<int> (Getdata (lines[1]));
		List<int> rareCard = new List<int> (Getdata (lines[2]));
		List<int> epicCard = new List<int> (Getdata (lines[3]));
		List<int> legendaryCard = new List<int> (Getdata (lines[4]));
		float commonWoodChestRatio;
		float.TryParse (lines[5], out commonWoodChestRatio);
		float rareWoodChestRatio;
		float.TryParse (lines[6], out rareWoodChestRatio);
		float epicWoodChestRatio;
		float.TryParse (lines[7], out epicWoodChestRatio);
		float legendaryWoodChestRatio;
		float.TryParse (lines[8], out legendaryWoodChestRatio);

		List<int> numberCardCommon = new List<int> (Getdata (lines[9]));
		List<int> numberCardRare = new List<int> (Getdata (lines[10]));
		List<int> numberCardEpic = new List<int> (Getdata (lines[11]));
		List<int> numberCardLegend = new List<int> (Getdata (lines[12]));

		float commonSilverChestRatio;
		float.TryParse (lines[13], out commonSilverChestRatio);
		float rareSilverChestRatio;
		float.TryParse (lines[14], out rareSilverChestRatio);
		float epicSilverChestRatio;
		float.TryParse (lines[15], out epicSilverChestRatio);
		float legendarySilverChestRatio;
		float.TryParse (lines[16], out legendarySilverChestRatio);

		List<int> numberCardCommonChestRare = new List<int> (Getdata (lines[17]));
		List<int> numberCardRareChestRare = new List<int> (Getdata (lines[18]));
		List<int> numberCardEpicChestRare = new List<int> (Getdata (lines[19]));
		List<int> numberCardLegendChestRare = new List<int> (Getdata (lines[20]));

		float commonGoldChestRatio;
		float.TryParse (lines[21], out commonGoldChestRatio);
		float rareGoldChestRatio;
		float.TryParse (lines[22], out rareGoldChestRatio);
		float epicGoldChestRatio;
		float.TryParse (lines[23], out epicGoldChestRatio);
		float legendaryGoldChestRatio;
		float.TryParse (lines[24], out legendaryGoldChestRatio);

		List<int> numberCardCommonChestEpic = new List<int> (Getdata (lines[25]));
		List<int> numberCardRareChestEpic = new List<int> (Getdata (lines[26]));
		List<int> numberCardEpicChestEpic = new List<int> (Getdata (lines[27]));
		List<int> numberCardLegendChestEpic = new List<int> (Getdata (lines[28]));

		return new ChestData (rangeLevel, commonCard, rareCard, epicCard, legendaryCard,
			commonWoodChestRatio, rareWoodChestRatio, epicWoodChestRatio, legendaryWoodChestRatio,
			numberCardCommon, numberCardRare, numberCardEpic, numberCardLegend,
			commonSilverChestRatio, rareSilverChestRatio, epicSilverChestRatio, legendarySilverChestRatio,
			numberCardCommonChestRare, numberCardRareChestRare, numberCardEpicChestRare, numberCardLegendChestRare,
			commonGoldChestRatio, rareGoldChestRatio, epicGoldChestRatio, legendaryGoldChestRatio,
			numberCardCommonChestEpic, numberCardRareChestEpic, numberCardEpicChestEpic, numberCardLegendChestEpic);
	}
	private static List<int> Getdata (string input)
	{
		var list = new List<int> ();
		input = input.Trim ();
		if (input.Length == 0)
		{
			return list;
		}
		var lines = Regex.Split (input, SPLIT_AT);
		for (int i = 0; i < lines.Length; i++)
		{
			int temp;
			int.TryParse (lines[i], out temp);
			list.Add (temp);
		}
		return list;
	}
}
