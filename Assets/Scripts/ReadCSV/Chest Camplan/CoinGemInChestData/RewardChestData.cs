﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RewardChestData {

	public int Level;
	public int Value;

	public RewardChestData (int level, int value)
	{
		Level = level;
		Value = value;
	}
}
