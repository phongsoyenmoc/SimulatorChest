﻿using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;

public class ReadCSVCoinExpRewardChest {

	public readonly static string SPLIT_RE = ",";
	public readonly static string SPLIT_AT = "&";
	public readonly static string LINE_SPLIT_RE = @"\n";
	public static Dictionary<int, RewardChestData> Read (TextAsset data)
	{
		var list = new Dictionary<int, RewardChestData> ();
		var lines = Regex.Split (data.text, LINE_SPLIT_RE);
		int length = lines.Length;
		if (length == 1)
			return list;
		for (int i = 1; i < lines.Length; i++)
		{
			list.Add (i, GetData (lines[i]));
		}
		return list;
	}

	public static Dictionary<int, RewardChestData> Read (string file)
	{
		TextAsset data = Resources.Load (file) as TextAsset;
		var list = Read (data);
		return list;
	}
	private static RewardChestData GetData (string input)
	{
		var lines = Regex.Split (input, SPLIT_RE);
		int level;
		int.TryParse (lines[0], out level);

		int value;
		int.TryParse (lines[1], out value);

		return new RewardChestData (level, value);
	}

	private static List<int> Getdata (string input)
	{
		var list = new List<int> ();
		input = input.Trim ();
		if (input.Length == 0)
		{
			return list;
		}
		var lines = Regex.Split (input, SPLIT_AT);
		for (int i = 0; i < lines.Length; i++)
		{
			int temp;
			int.TryParse (lines[i], out temp);
			list.Add (temp);
		}
		return list;
	}
}
