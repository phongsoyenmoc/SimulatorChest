﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinGemInChestShopData
{
	private List<int> rangeLevel;
	private int freeChestCoin;
	private int rareChestCoin;
	private int epicChestCoin;
	private int legendaryChestCoin;

	private int freeChestGem;
	private int rareChestGem;
	private int epicChestGem;
	private int legendaryChestGem;

	public List<int> RangeLevel
	{
		get {
			return rangeLevel;
		}

		set {
			rangeLevel = value;
		}
	}

	public int FreeChestCoin
	{
		get {
			return freeChestCoin;
		}

		set {
			freeChestCoin = value;
		}
	}

	public int RareChestCoin
	{
		get {
			return rareChestCoin;
		}

		set {
			rareChestCoin = value;
		}
	}

	public int EpicChestCoin
	{
		get {
			return epicChestCoin;
		}

		set {
			epicChestCoin = value;
		}
	}

	public int LegendaryChestCoin
	{
		get {
			return legendaryChestCoin;
		}

		set {
			legendaryChestCoin = value;
		}
	}

	public int FreeChestGem
	{
		get {
			return freeChestGem;
		}

		set {
			freeChestGem = value;
		}
	}

	public int RareChestGem
	{
		get {
			return rareChestGem;
		}

		set {
			rareChestGem = value;
		}
	}

	public int EpicChestGem
	{
		get {
			return epicChestGem;
		}

		set {
			epicChestGem = value;
		}
	}

	public int LegendaryChestGem
	{
		get {
			return legendaryChestGem;
		}

		set {
			legendaryChestGem = value;
		}
	}

	public CoinGemInChestShopData ()
	{

	}
	public CoinGemInChestShopData(List<int> _rangeLevel, int _freeCoin, int _rareCoin, int _epicCoin,int _legendCoin, int _freeGem, int _rareGem, int _epicGem, int _legendGem)
	{
		RangeLevel = _rangeLevel;
		FreeChestCoin = _freeCoin;
		RareChestCoin = _rareCoin;
		EpicChestCoin = _epicCoin;
		LegendaryChestCoin = _legendCoin;
		FreeChestGem = _freeGem;
		RareChestGem = _rareGem;
		EpicChestGem = _epicGem;
		LegendaryChestGem = _legendGem;
	
	}
}
