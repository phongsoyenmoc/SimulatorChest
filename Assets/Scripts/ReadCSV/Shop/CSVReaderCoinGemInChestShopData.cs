﻿using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;

public class CSVReaderCoinGemInChestShopData {

	// Use this for initialization
	public readonly static string SPLIT_RE = ",";
	public readonly static string SPLIT_AT = "&";
	public readonly static string LINE_SPLIT_RE = @"\n";
	public static Dictionary<int, CoinGemInChestShopData> Read (TextAsset data)
	{
		var list = new Dictionary<int, CoinGemInChestShopData> ();
		var lines = Regex.Split (data.text, LINE_SPLIT_RE);
		int length = lines.Length;
		if (length == 1)
			return list;
		for (int i = 1; i < lines.Length; i++)
		{
			list.Add (i, GetData (lines[i]));
		}
		return list;
	}

	public static Dictionary<int, CoinGemInChestShopData> Read (string file)
	{
		TextAsset data = Resources.Load (file) as TextAsset;
		var list = Read (data);
		return list;
	}

	private static CoinGemInChestShopData GetData (string input)
	{
		var lines = Regex.Split (input, SPLIT_RE);
		List<int> _rangeLevel = new List<int> (Getdata (lines[0]));
		int freeCoin;
		int.TryParse (lines[1], out freeCoin);
		int rareCoin;
		int.TryParse (lines[2], out rareCoin);
		int epicCoin;
		int.TryParse (lines[3], out epicCoin);
		int legendCoin;
		int.TryParse (lines[4], out legendCoin);
		int freeGem;
		int.TryParse (lines[5], out freeGem);
		int rareGem;
		int.TryParse (lines[6], out rareGem);
		int epicGem;
		int.TryParse (lines[7], out epicGem);
		int legendGem;
		int.TryParse (lines[8], out legendGem);
		return new CoinGemInChestShopData (_rangeLevel, freeCoin, rareCoin, epicCoin, legendCoin, freeGem, rareGem, epicGem, legendGem);
	}


	private static List<int> Getdata (string input)
	{
		var list = new List<int> ();
		input = input.Trim ();
		if (input.Length == 0)
		{
			return list;
		}
		var lines = Regex.Split (input, SPLIT_AT);
		for (int i = 0; i < lines.Length; i++)
		{
			int temp;
			int.TryParse (lines[i], out temp);
			list.Add (temp);
		}
		return list;
	}

}
