﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyInChestShopData
{
	private int idCommon;
	private int idRare;
	private int idEpic;
	private int idLegendary;

	private List<int> rateCommonReduction;
	private List<int> rateRareReduction;
	private List<int> rateEpicReduction;
	private List<int> rateLegendaryReduction;

	public int IdCommon
	{
		get {
			return idCommon;
		}

		set {
			idCommon = value;
		}
	}

	public int IdRare
	{
		get {
			return idRare;
		}

		set {
			idRare = value;
		}
	}

	public int IdEpic
	{
		get {
			return idEpic;
		}

		set {
			idEpic = value;
		}
	}

	public int IdLegendary
	{
		get {
			return idLegendary;
		}

		set {
			idLegendary = value;
		}
	}

	public List<int> RateCommonReduction
	{
		get {
			return rateCommonReduction;
		}

		set {
			rateCommonReduction = value;
		}
	}

	public List<int> RateRareReduction
	{
		get {
			return rateRareReduction;
		}

		set {
			rateRareReduction = value;
		}
	}

	public List<int> RateEpicReduction
	{
		get {
			return rateEpicReduction;
		}

		set {
			rateEpicReduction = value;
		}
	}

	public List<int> RateLegendaryReduction
	{
		get {
			return rateLegendaryReduction;
		}

		set {
			rateLegendaryReduction = value;
		}
	}


	public EnemyInChestShopData ()
	{

	}
	public EnemyInChestShopData(int _idCommon, List<int> _rateCommon, int _idRare, List<int> _rateRare, int _idEpic, List<int> _rateEpic, int _idLegend, List<int> _rateLegend)
	{
		this.IdCommon = _idCommon;
		this.IdRare = _idRare;
		this.IdEpic = _idEpic;
		this.IdLegendary = _idLegend;
		this.RateCommonReduction = _rateCommon;
		this.RateRareReduction = _rateRare;
		this.RateEpicReduction = _rateEpic;
		this.RateLegendaryReduction = _rateLegend;
	}
}
