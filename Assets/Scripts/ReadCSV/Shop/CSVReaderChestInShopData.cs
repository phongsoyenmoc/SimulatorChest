﻿using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;

public class CSVReaderChestInShopData
{

	public readonly static string SPLIT_RE = ",";
	public readonly static string SPLIT_AT = "&";
	public readonly static string LINE_SPLIT_RE = @"\n";

	public static Dictionary<int, ChestInShopData> Read (TextAsset data)
	{
		var list = new Dictionary<int, ChestInShopData> ();
		var lines = Regex.Split (data.text, LINE_SPLIT_RE);
		int length = lines.Length;
		if (length == 1)
			return list;
		for (int i = 1; i < lines.Length; i++)
		{
			list.Add (i, GetChestData (lines[i]));
		}
		return list;
	}

	public static Dictionary<int, ChestInShopData> Read (string file)
	{
		TextAsset data = Resources.Load (file) as TextAsset;
		var list = Read (data);
		return list;
	}

	private static ChestInShopData GetChestData (string input)
	{
		var lines = Regex.Split (input, SPLIT_RE);
		List<int> rangeLevel = new List<int> (Getdata (lines[0]));
		List<int> commonCards = new List<int> (Getdata (lines[1]));
		List<int> rareCards = new List<int> (Getdata (lines[2]));
		List<int> epicCards = new List<int> (Getdata (lines[3]));
		List<int> legendCards = new List<int> (Getdata (lines[4]));
		int freeCommonRate;
		int.TryParse (lines[5], out freeCommonRate);
		int freeRareRate;
		int.TryParse (lines[6], out freeRareRate);
		int freeEpicRate;
		int.TryParse (lines[7], out freeEpicRate);
		int freeLegendRate;
		int.TryParse (lines[8], out freeLegendRate);

		List<int> numberCardCommon = new List<int> (Getdata (lines[9]));
		List<int> numberCardRare = new List<int> (Getdata (lines[10]));
		List<int> numberCardEpic = new List<int> (Getdata (lines[11]));
		List<int> numberCardLegend = new List<int> (Getdata (lines[12]));

		int blueCommonRate;
		int.TryParse (lines[13], out blueCommonRate);
		int blueRareRate;
		int.TryParse (lines[14], out blueRareRate);
		int blueEpicRate;
		int.TryParse (lines[15], out blueEpicRate);
		int blueLegendRate;
		int.TryParse (lines[16], out blueLegendRate);

		List<int> numberCardCommonChestRare = new List<int> (Getdata (lines[17]));
		List<int> numberCardRareChestRare = new List<int> (Getdata (lines[18]));
		List<int> numberCardEpicChestRare = new List<int> (Getdata (lines[19]));
		List<int> numberCardLegendChestRare = new List<int> (Getdata (lines[20]));

		int violetCommonRate;
		int.TryParse (lines[21], out violetCommonRate);
		int violetRareRate;
		int.TryParse (lines[22], out violetRareRate);
		int violetEpicRate;
		int.TryParse (lines[23], out violetEpicRate);
		int violetLegendRate;
		int.TryParse (lines[24], out violetLegendRate);

		List<int> numberCardCommonChestEpic = new List<int> (Getdata (lines[25]));
		List<int> numberCardRareChestEpic = new List<int> (Getdata (lines[26]));
		List<int> numberCardEpicChestEpic = new List<int> (Getdata (lines[27]));
		List<int> numberCardLegendChestEpic = new List<int> (Getdata (lines[28]));

		int redCommonRate;
		int.TryParse (lines[29], out redCommonRate);
		int redRareRate;
		int.TryParse (lines[30], out redRareRate);
		int redEpicRate;
		int.TryParse (lines[31], out redEpicRate);
		int redLegendRate;
		int.TryParse (lines[32], out redLegendRate);

		List<int> numberCardCommonChestLegend = new List<int> (Getdata (lines[33]));
		List<int> numberCardRareChestLegend = new List<int> (Getdata (lines[34]));
		List<int> numberCardEpicChestLegend = new List<int> (Getdata (lines[35]));
		List<int> numberCardLegendChestLegend = new List<int> (Getdata (lines[36]));
		return new ChestInShopData (rangeLevel,
			commonCards,
			rareCards,
			epicCards,
			legendCards,
			freeCommonRate,
			freeRareRate,
			freeEpicRate,
			freeLegendRate,

			numberCardCommon,
			numberCardRare,
			numberCardEpic,
			numberCardLegend,

			blueCommonRate,
			blueRareRate,
			blueEpicRate,
			blueLegendRate,

			numberCardCommonChestRare,
			numberCardRareChestRare,
			numberCardEpicChestRare,
			numberCardLegendChestRare,

			violetCommonRate,
			violetRareRate,
			violetEpicRate,
			violetLegendRate,

			numberCardCommonChestEpic,
			numberCardRareChestEpic,
			numberCardEpicChestEpic,
			numberCardLegendChestEpic,

			redCommonRate,
			redRareRate,
			redEpicRate,
			redLegendRate,

			numberCardCommonChestLegend,
			numberCardRareChestLegend,
			numberCardEpicChestLegend,
			numberCardLegendChestLegend);
	}

	private static List<int> Getdata (string input)
	{
		var list = new List<int> ();
		input = input.Trim ();
		if (input.Length == 0)
		{
			return list;
		}
		var lines = Regex.Split (input, SPLIT_AT);
		for (int i = 0; i < lines.Length; i++)
		{
			int temp;
			int.TryParse (lines[i], out temp);
			list.Add (temp);
		}
		return list;
	}
}
