﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChestInShopData
{
	private List<int> rangeLevel;

	private List<int> commonCards;
	private List<int> rareCards;
	private List<int> epicCards;
	private List<int> legendCards;

	//free
	private int freeCommonRate;
	private int freeRareRate;
	private int freeEpicRate;
	private int freeLegendRate;

	private List<int> numberCardCommon;
	private List<int> numberCardRare;
	private List<int> numberCardEpic;
	private List<int> numberCardLegend;

	//rare
	private int blueCommonRate;
	private int blueRareRate;
	private int blueEpicRate;
	private int blueLegendRate;

	private List<int> numberCardCommonChestRare;
	private List<int> numberCardRareChestRare;
	private List<int> numberCardEpicChestRare;
	private List<int> numberCardLegendChestRare;

	//epic
	private int violetCommonRate;
	private int violetRareRate;
	private int violetEpicRate;
	private int violetLegendRate;

	private List<int> numberCardCommonChestEpic;
	private List<int> numberCardRareChestEpic;
	private List<int> numberCardEpicChestEpic;
	private List<int> numberCardLegendChestEpic;


	//legend
	private int redCommonRate;
	private int redRareRate;
	private int redEpicRate;
	private int redLegendRate;

	private List<int> numberCardCommonChestLegend;
	private List<int> numberCardRareChestLegend;
	private List<int> numberCardEpicChestLegend;
	private List<int> numberCardLegendChestLegend;

	public List<int> RangeLevel
	{
		get {
			return rangeLevel;
		}

		set {
			rangeLevel = value;
		}
	}

	public List<int> CommonCards
	{
		get {
			return commonCards;
		}

		set {
			commonCards = value;
		}
	}

	public List<int> RareCards
	{
		get {
			return rareCards;
		}

		set {
			rareCards = value;
		}
	}

	public List<int> EpicCards
	{
		get {
			return epicCards;
		}

		set {
			epicCards = value;
		}
	}

	public List<int> LegendCards
	{
		get {
			return legendCards;
		}

		set {
			legendCards = value;
		}
	}

	public int FreeCommonRate
	{
		get {
			return freeCommonRate;
		}

		set {
			freeCommonRate = value;
		}
	}

	public int FreeRareRate
	{
		get {
			return freeRareRate;
		}

		set {
			freeRareRate = value;
		}
	}

	public int FreeLegendRate
	{
		get {
			return freeLegendRate;
		}

		set {
			freeLegendRate = value;
		}
	}

	public int FreeEpicRate
	{
		get {
			return freeEpicRate;
		}

		set {
			freeEpicRate = value;
		}
	}

	public List<int> NumberCardCommon
	{
		get {
			return numberCardCommon;
		}

		set {
			numberCardCommon = value;
		}
	}

	public List<int> NumberCardRare
	{
		get {
			return numberCardRare;
		}

		set {
			numberCardRare = value;
		}
	}

	public List<int> NumberCardEpic
	{
		get {
			return numberCardEpic;
		}

		set {
			numberCardEpic = value;
		}
	}

	public List<int> NumberCardLegend
	{
		get {
			return numberCardLegend;
		}

		set {
			numberCardLegend = value;
		}
	}

	public int BlueCommonRate
	{
		get {
			return blueCommonRate;
		}

		set {
			blueCommonRate = value;
		}
	}

	public int BlueRareRate
	{
		get {
			return blueRareRate;
		}

		set {
			blueRareRate = value;
		}
	}

	public int BlueEpicRate
	{
		get {
			return blueEpicRate;
		}

		set {
			blueEpicRate = value;
		}
	}

	public int BlueLegendRate
	{
		get {
			return blueLegendRate;
		}

		set {
			blueLegendRate = value;
		}
	}

	public List<int> NumberCardCommonChestRare
	{
		get {
			return numberCardCommonChestRare;
		}

		set {
			numberCardCommonChestRare = value;
		}
	}

	public List<int> NumberCardRareChestRare
	{
		get {
			return numberCardRareChestRare;
		}

		set {
			numberCardRareChestRare = value;
		}
	}

	public List<int> NumberCardEpicChestRare
	{
		get {
			return numberCardEpicChestRare;
		}

		set {
			numberCardEpicChestRare = value;
		}
	}

	public List<int> NumberCardLegendChestRare
	{
		get {
			return numberCardLegendChestRare;
		}

		set {
			numberCardLegendChestRare = value;
		}
	}

	public int VioletCommonRate
	{
		get {
			return violetCommonRate;
		}

		set {
			violetCommonRate = value;
		}
	}

	public int VioletRareRate
	{
		get {
			return violetRareRate;
		}

		set {
			violetRareRate = value;
		}
	}

	public int VioletEpicRate
	{
		get {
			return violetEpicRate;
		}

		set {
			violetEpicRate = value;
		}
	}

	public int VioletLegendRate
	{
		get {
			return violetLegendRate;
		}

		set {
			violetLegendRate = value;
		}
	}

	public List<int> NumberCardCommonChestEpic
	{
		get {
			return numberCardCommonChestEpic;
		}

		set {
			numberCardCommonChestEpic = value;
		}
	}

	public List<int> NumberCardRareChestEpic
	{
		get {
			return numberCardRareChestEpic;
		}

		set {
			numberCardRareChestEpic = value;
		}
	}

	public List<int> NumberCardEpicChestEpic
	{
		get {
			return numberCardEpicChestEpic;
		}

		set {
			numberCardEpicChestEpic = value;
		}
	}

	public List<int> NumberCardLegendChestEpic
	{
		get {
			return numberCardLegendChestEpic;
		}

		set {
			numberCardLegendChestEpic = value;
		}
	}

	public int RedCommonRate
	{
		get {
			return redCommonRate;
		}

		set {
			redCommonRate = value;
		}
	}

	public int RedRareRate
	{
		get {
			return redRareRate;
		}

		set {
			redRareRate = value;
		}
	}

	public int RedEpicRate
	{
		get {
			return redEpicRate;
		}

		set {
			redEpicRate = value;
		}
	}

	public int RedLegendRate
	{
		get {
			return redLegendRate;
		}

		set {
			redLegendRate = value;
		}
	}

	public List<int> NumberCardCommonChestLegend
	{
		get {
			return numberCardCommonChestLegend;
		}

		set {
			numberCardCommonChestLegend = value;
		}
	}

	public List<int> NumberCardRareChestLegend
	{
		get {
			return numberCardRareChestLegend;
		}

		set {
			numberCardRareChestLegend = value;
		}
	}

	public List<int> NumberCardEpicChestLegend
	{
		get {
			return numberCardEpicChestLegend;
		}

		set {
			numberCardEpicChestLegend = value;
		}
	}

	public List<int> NumberCardLegendChestLegend
	{
		get {
			return numberCardLegendChestLegend;
		}

		set {
			numberCardLegendChestLegend = value;
		}
	}

	public ChestInShopData ()
	{

	}

	public ChestInShopData (List<int> rangeLevel,
		List<int> commonCards,
		List<int> rareCards,
		List<int> epicCards,
		List<int> legendCards,
		int freeCommonRate,
		int freeRareRate,
		int freeEpicRate,
		int freeLegendRate,

		List<int> numberCardCommon,
		List<int> numberCardRare,
		List<int> numberCardEpic,
		List<int> numberCardLegend,

		int blueCommonRate,
		int blueRareRate,
		int blueEpicRate,
		int blueLegendRate,

		List<int> numberCardCommonChestRare,
		List<int> numberCardRareChestRare,
		List<int> numberCardEpicChestRare,
		List<int> numberCardLegendChestRare,

		 int violetCommonRate,
		 int violetRareRate,
		 int violetEpicRate,
		 int violetLegendRate,

		 List<int> numberCardCommonChestEpic,
		 List<int> numberCardRareChestEpic,
		 List<int> numberCardEpicChestEpic,
		 List<int> numberCardLegendChestEpic,

		 int redCommonRate,
		 int redRareRate,
		 int redEpicRate,
		 int redLegendRate,

		 List<int> numberCardCommonChestLegend,
		 List<int> numberCardRareChestLegend,
		 List<int> numberCardEpicChestLegend,
		 List<int> numberCardLegendChestLegend)
	{

		this.rangeLevel = rangeLevel;
		this.commonCards = commonCards;
		this.rareCards = rareCards;
		this.epicCards = epicCards;
		this.legendCards = legendCards;
		this.freeCommonRate = freeCommonRate;
		this.freeRareRate = freeRareRate;
		this.freeEpicRate = freeEpicRate;
		this.freeLegendRate = freeLegendRate;

		this.numberCardCommon = numberCardCommon;
		this.numberCardRare = numberCardRare;
		this.numberCardEpic = numberCardEpic;
		this.numberCardLegend = numberCardLegend;

		this.blueCommonRate = blueCommonRate;
		this.blueRareRate = blueRareRate;
		this.blueEpicRate = blueEpicRate;
		this.blueLegendRate = blueLegendRate;

		this.numberCardCommonChestRare = numberCardCommonChestRare;
		this.numberCardRareChestRare = numberCardRareChestRare;
		this.numberCardEpicChestRare = numberCardEpicChestRare;
		this.numberCardLegendChestRare = numberCardLegendChestRare;

		this.violetCommonRate = violetCommonRate;
		this.violetRareRate = violetRareRate;
		this.violetEpicRate = violetEpicRate;
		this.violetLegendRate = violetLegendRate;

		this.numberCardCommonChestEpic = numberCardCommonChestEpic;
		this.numberCardRareChestEpic = numberCardRareChestEpic;
		this.numberCardEpicChestEpic = numberCardEpicChestEpic;
		this.numberCardLegendChestEpic = numberCardLegendChestEpic;

		this.redCommonRate = redCommonRate;
		this.redRareRate = redRareRate;
		this.redEpicRate = redEpicRate;
		this.redLegendRate = redLegendRate;

		this.numberCardCommonChestLegend = numberCardCommonChestLegend;
		this.numberCardRareChestLegend = numberCardRareChestLegend;
		this.numberCardEpicChestLegend = numberCardEpicChestLegend;
		this.numberCardLegendChestLegend = numberCardLegendChestLegend;
	}


	public void ShowData ()
	{
		//foreach (var item in rangeLevel)
		//{
		//	Debug.Log ("range level : " + item);
		//}

		//foreach (var item in commonCards)
		//{
		//	Debug.Log ("common card : " + item);
		//}
		//foreach (var item in rareCards)
		//{
		//	Debug.Log ("rare card : " + item);
		//}
		//foreach (var item in epicCards)
		//{
		//	Debug.Log ("epic card : " + item);
		//}
		//foreach (var item in legendCards)
		//{
		//	Debug.Log ("legend card : " + item);
		//}

		//Debug.Log ("freeCommonRate : " + freeCommonRate);
		//Debug.Log ("freeRareRate : " + freeRareRate);
		//Debug.Log ("freeEpicRate : " + freeEpicRate);
		//Debug.Log ("freeLegendRate : " + freeLegendRate);


		//foreach (var item in numberCardCommon)
		//{
		//	Debug.Log ("number card common : " + item);
		//}
		//foreach (var item in numberCardRare)
		//{
		//	Debug.Log ("number card rare : " + item);
		//}
		//foreach (var item in numberCardEpic)
		//{
		//	Debug.Log ("number card epic: " + item);
		//}
		//foreach (var item in numberCardLegend)
		//{
		//	Debug.Log ("number card legend : " + item);
		//}
	}


}
