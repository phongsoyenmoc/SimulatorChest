﻿using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;

public class CSVReaderEnemyInChestShopData {

	// Use this for initialization
	public readonly static string SPLIT_RE = ",";
	public readonly static string SPLIT_AT = "&";
	public readonly static string LINE_SPLIT_RE = @"\n";
	public static Dictionary<int, EnemyInChestShopData> Read (TextAsset data)
	{
		var list = new Dictionary<int, EnemyInChestShopData> ();
		var lines = Regex.Split (data.text, LINE_SPLIT_RE);
		int length = lines.Length;
		if (length == 1)
			return list;
		for (int i = 1; i < lines.Length; i++)
		{
			list.Add (i, GetData (lines[i]));
		}
		return list;
	}

	public static Dictionary<int, EnemyInChestShopData> Read (string file)
	{
		TextAsset data = Resources.Load (file) as TextAsset;
		var list = Read (data);
		return list;
	}

	private static EnemyInChestShopData GetData (string input)
	{
		var lines = Regex.Split (input, SPLIT_RE);
		int idCommon;
		int.TryParse (lines[0], out idCommon);
		List<int> rateCommon = new List<int> (Getdata (lines[1]));
		int idRare;
		int.TryParse (lines[2], out idRare);
		List<int> rateRare = new List<int> (Getdata (lines[3]));
		int idEpic;
		int.TryParse (lines[4], out idEpic);
		List<int> rateEpic = new List<int> (Getdata (lines[5]));
		int idLegend;
		int.TryParse (lines[6], out idLegend);
		List<int> rateLegend = new List<int> (Getdata (lines[7]));

		return new EnemyInChestShopData (idCommon, rateCommon, idRare, rateRare, idEpic, rateEpic, idLegend, rateLegend);
	}


	private static List<int> Getdata (string input)
	{
		var list = new List<int> ();
		input = input.Trim ();
		if (input.Length == 0)
		{
			return list;
		}
		var lines = Regex.Split (input, SPLIT_AT);
		for (int i = 0; i < lines.Length; i++)
		{
			int temp;
			int.TryParse (lines[i], out temp);
			list.Add (temp);
		}
		return list;
	}
}
