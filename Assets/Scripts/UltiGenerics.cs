﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;

public class UltiGenerics
{

	public static bool ExistIdInList (int id, List<int> list)
	{
		if (list.Count == 0)
		{
			return false;
		}
		foreach (var item in list)
		{
			if (item == id)
			{
				return true;
			}
		}
		return false;
	}


	public static bool ExistInListExcept (int id, List<int> list, int idExcept)
	{
		if (list.Count == 0)
		{
			return false;
		}
		if (id == idExcept)
		{
			return false;
		}

		foreach (var item in list)
		{
			if (item == id)
			{
				return true;
			}
		}
		return false;
	}

	public static bool ExistIdInArray (int id, int[] array)
	{
		if (array.Length == 0)
		{
			return false;
		}
		foreach (var item in array)
		{
			if (item == id)
			{
				return true;
			}
		}
		return false;
	}




	static bool Compare<T> (T x, T y)
	{
		return EqualityComparer<T>.Default.Equals (x, y);
	}

	public static bool ExistInList<T> (ref T root, ref List<T> target)
	{
		if (target.Count == 0)
		{
			return false;
		}
		foreach (var item in target)
		{
			if (Compare<T> (item, root))
			{
				return true;
			}
		}

		return false;
	}



	public static bool RandomMaxTwice (float num)
	{
		float a = UnityEngine.Random.Range (0f, 100f);
		float b = UnityEngine.Random.Range (0f, 100f);
		return (Mathf.Max (a, b) < num);
	}

	public static bool RandomMinTwice (float num)
	{
		float a = UnityEngine.Random.Range (0f, 100f);
		float b = UnityEngine.Random.Range (0f, 100f);
		return (Mathf.Min (a, b) < num);
	}

	public static float RandomMinTwice ()
	{
		float a = UnityEngine.Random.Range (0f, 100f);
		float b = UnityEngine.Random.Range (0f, 100f);
		return Mathf.Min (a, b);
	}

	public static float RandomMaxTwice ()
	{
		float a = UnityEngine.Random.Range (0f, 100f);
		float b = UnityEngine.Random.Range (0f, 100f);
		return Mathf.Max (a, b);
	}

	public static float RandomMaxSpecal (float min, float max)
	{
		float roll1 = UnityEngine.Random.Range (min, max / 2f);
		float roll2 = UnityEngine.Random.Range (min, max / 2f);
		float roll3 = UnityEngine.Random.Range (min, max / 2f);

		float roll = roll1 + roll2 + roll3;

		return roll - Mathf.Min (roll1, roll2, roll3) + UnityEngine.Random.Range (min, max / 2f);

	}

	public static int GetRandomItemInList (List<int> listIds)
	{
		int rand = UnityEngine.Random.Range (0, listIds.Count);
		return listIds[rand];
	}

	public static Transform GetRandomItemInList (List<Transform> listTransforms)
	{
		int rand = UnityEngine.Random.Range (0, listTransforms.Count);
		return listTransforms[rand];
	}

	public static T GetRandomItemInList<T> (List<T> listTransforms)
	{
		int rand = UnityEngine.Random.Range (0, listTransforms.Count);
		return listTransforms[rand];
	}


	/// <summary>
	/// identifi co dang type_.....
	/// </summary>
	/// <param name="identifi"></param>
	/// <returns></returns>
	public static string TypeItem (string identifi)
	{
		List<string> datas = identifi.Split ('_').ToList ();
		return datas[0];
	}

	public static string TypeItemLast (string identifi)
	{
		List<string> datas = identifi.Split ('_').ToList ();
		return datas.Last ();
	}



	/// <summary>
	/// Split string format hh:mm:ss to list hh , mm, ss
	/// </summary>
	/// <param name="_time"></param>
	/// <returns></returns>
	public static List<string> SplitCurrentTime (string _time)
	{
		return _time.Split (':').ToList ();
	}
	public static System.TimeSpan AddingTimeCooldown (string _currentTime, string _timeValue)
	{
		List<string> currents = SplitCurrentTime (_currentTime);
		List<string> values = SplitCurrentTime (_timeValue);

		int hours = int.Parse (currents[0]) + int.Parse (values[0]);
		int minutes = int.Parse (currents[1]) + int.Parse (values[1]);
		int seconds = int.Parse (currents[2]) + int.Parse (values[2]);

		while (seconds >= 60)
		{
			seconds -= 60;
			minutes++;
		}
		while (minutes >= 60)
		{
			minutes -= 60;
			hours++;
		}
		while (hours >= 24)
		{
			hours = 23;
			minutes = 59;
			seconds = 59;
		}

		return System.TimeSpan.Parse (hours + ":" + minutes + ":" + seconds);
	}

	/// <summary>
	/// Split by '-'
	/// </summary>
	/// <param name="_infoLine"></param>
	/// <returns></returns>
	public static List<string> SplitSkillInfo (string _infoLine)
	{
		return _infoLine.Split ('-').ToList ();
	}



	public static Transform GetTransformDiffirent (Transform target, List<Transform> container)
	{
		if (container.Count > 1)
		{
			List<Transform> trans = new List<Transform> ();
			foreach (var item in container)
			{
				if (item != target)
				{
					trans.Add (item);
				}
			}
			return GetRandomItemInList (trans);
		}
		return null;

	}

	/// <summary>
	/// compare a / b realy = rate
	/// </summary>
	/// <param name="rate"></param>
	/// <param name="a"></param>
	/// <param name="b"></param>
	/// <returns></returns>
	public static bool CompareRateNumber (float rate, float a, float b)
	{
		return ((a / b) * 100) <= rate;
	}


	public static bool ExistInDictionary (Dictionary<int, int> dict, int _key)
	{
		foreach (var item in dict.Keys)
		{
			if (_key == item)
			{
				return true;
			}
		}
		return false;
	}



	public static string ConvertNumberproperty (float currentValue, float slow, float normal, float fast, bool isReversibles = false)
	{
		if (!isReversibles)
		{
			if (currentValue <= slow)
			{
				return "Slow";
			}

			if (currentValue <= normal)
			{
				return "Normal";
			}

			if (currentValue <= fast)
			{
				return "Fast";
			}
			return "Very fast";
		}
		else
		{
			if (currentValue <= fast)
			{
				return "Fast";
			}

			if (currentValue <= normal)
			{
				return "Normal";
			}

			if (currentValue <= slow)
			{
				return "Slow";
			}
			return "Very Slow";
		}

	}


	public static float GetY (Vector2 point1, Vector2 point2, float x)
	{
		var m = (point2.y - point1.y) / (point2.x - point1.x);
		var b = point1.y - (m * point1.x);

		return m * x + b;
	}


	public static List<T> Shuffle<T> (List<T> list)
	{
		System.Random rand = new System.Random ();
		return list.OrderBy (c => rand.Next ()).ToList ();
	}


	public static List<T> Except<T> (List<T> container, List<T> tempo)
	{
		return container.Except (tempo).ToList ();
	}


	/// <summary>
	/// random doubleNumber form minNumber to MaxNumber
	/// </summary>
	/// <param name="rand"></param>
	/// <param name="minNumber"></param>
	/// <param name="maxNumber"></param>
	/// <returns></returns>
	public static double Randomlize (System.Random rand, double minNumber, double maxNumber)
	{
		return minNumber + rand.NextDouble () * (maxNumber - minNumber);
	}



	/// <summary>
	/// not has max value same unity random
	/// </summary>
	/// <param name="min"></param>
	/// <param name="max"></param>
	/// <returns></returns>
	public static int RandomlizeMin (int min, int max)
	{
		int a = RandomNumber.GenerateRandom (min, max);
		int b = RandomNumber.GenerateRandom (min, max);
		return Mathf.Min (a, b);
	}

	/// <summary>
	/// not has max value same unity random
	/// </summary>
	/// <param name="min"></param>
	/// <param name="max"></param>
	/// <returns></returns>
	public static float RandomlizeMax (int min, int max)
	{
		float a = RandomNumber.GenerateRandom (min, max);
		float b = RandomNumber.GenerateRandom (min, max);
		return Mathf.Max (a, b);
	}


	public static List<int> MakeList (int count, int maxNumberCard)
	{
		List<int> lists = new List<int> ();
		if (count == 0)
		{
			return new List<int> ();
		}
		else
		{
			do
			{
				int value = (int) (maxNumberCard / count);
				maxNumberCard -= value;
				count -= 1;
				lists.Add (value);
			} while (count >= 1);
			return lists;
		}
	}

	//public static Dictionary<int, int> MakeDictionary (List<int> keys, List<int> values)
	//{
	//	return keys.Zip (values, (k, v) => new { k, v })
	//		  .ToDictionary (x => x.k, x => x.v);
	//}

	/// <summary>
	/// cacaulate rate count integer has sum  = sum 
	/// </summary>
	/// <param name="count"></param>
	/// <param name="sum"></param>
	/// <param name="minValue"></param>
	/// <returns></returns>
	public static List<float> RateWithCount (int count, int sum, int minValue)
	{
		float[] tempo = new float[count];
		for (int i = 0; i < count; i++)
		{
			tempo[i] = UnityEngine.Random.Range (minValue, 100);
		}

		float coe = tempo.Sum () / sum;
		for (int i = 0; i < count - 1; i++)
		{
			tempo[i] = Convert.ToInt32 (tempo[i] / coe);
		}
		tempo[count - 1] = 0;
		tempo[count - 1] = sum - tempo.Sum ();

		return tempo.OrderByDescending (foo => foo).ToList ();
	}


	public static List<T1> GetKeysDictionary<T1, T2> (Dictionary<T1, T2> dictionary)
	{
		return dictionary.Keys.ToList ();
	}

	public static List<T2> GetValuesDictionary<T1, T2> (Dictionary<T1, T2> dictionary)
	{
		return dictionary.Values.ToList ();
	}


	/// <summary>
	/// return list have number card for each id enemy
	/// </summary>
	/// <param name="maxCount">count card get</param>
	/// <param name="maxNumberCard">sum card get for count card</param>
	/// <param name="containerRate"></param>
	/// <returns></returns>
	public static List<int> CaculateNumberCardFollowRate (int maxCount, int maxNumberCard, List<float> containerRate)
	{
		List<int> resultNumber = new List<int> ();
		for (int i = 0; i < maxCount - 1; i++)
		{
			resultNumber.Add ((int) (maxNumberCard * containerRate[i] / 100));
		}
		resultNumber.Add (maxNumberCard - resultNumber.Sum ());

		return resultNumber;
	}


	public static Dictionary<T1, T2> Shuffle<T1, T2> (Dictionary<T1, T2> source)
	{
		System.Random r = new System.Random ();
		return source.OrderBy (x => r.Next ())
		   .ToDictionary (item => item.Key, item => item.Value);
	}

	public static Dictionary<TKey, TValue> MakeDictionary<TKey, TValue> (List<TKey> keys, List<TValue> values)
	{
		return keys.Zip (values, (k, v) => new { k, v }).ToDictionary (x => x.k, x => x.v);
	}


	/// <summary>
	/// Merging data form dictionaryB to dictionaryA
	/// </summary>
	/// <typeparam name="TKey"></typeparam>
	/// <typeparam name="TValue"></typeparam>
	/// <param name="dictionaryA"></param>
	/// <param name="dictionaryB"></param>
	public static void MergingDictionary (ref Dictionary<int, int> dictionaryA, Dictionary<int, int> dictionaryB)
	{
		//List<int> KeyOnlyDictionaryB =  GetKeysDictionary(dictionaryA).Where (p => !GetKeysDictionary(dictionaryB).Any (p2=>p2 == p)).ToList();

		foreach (var item in dictionaryB.Keys)
		{
			if (!ExistIdInList(item, GetKeysDictionary (dictionaryA)))
			{
				dictionaryA.Add (item, dictionaryB[item]);
			}
			else
			{
				//da co trong dcitionary A
				dictionaryA[item] += dictionaryB[item];
			}
		}

	}

	
}
